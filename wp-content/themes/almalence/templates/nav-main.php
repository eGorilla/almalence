<nav class="nav-main">
    <?php
    wp_nav_menu(array(
        'theme_location'    => 'primary',
        'container'         => false,
        'items_wrap'        => '<ul class="ul-main">%3$s</ul>'
    ));
    ?>
    <ul class="ul-helper"></ul>
    <ul class="ul-dummy"></ul>
    <a class="a-bars"></a>
</nav>