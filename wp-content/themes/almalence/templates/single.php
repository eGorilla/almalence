<?php
$ID = get_the_ID();
$tags = wp_get_post_tags($ID);
?>
<div class="container">
    <article id="post-<?=$ID;?>" class="single">
        <header>
            <h1><?php viewPostShare($ID); ?><?=$post->post_title;?></h1>
            <?php viewPostCategories(); ?>
            <?php viewPostTags($tags); ?>

            <ul class="list" style="display: none">
                <li><?=viewPostViews($ID);?></li>
                <li><?=viewPostCommentsCount();?></li>
            </ul>
        </header>

        <div class="body">
            <?=$post->post_content;?>
        </div>
    </article>
</div>
<?php
$amount = 0;
$topicPin = false;
$a = [];
$topic = $_SESSION && isset($_SESSION["topic"]) ? $_SESSION["topic"] : false;
if ($topic) {
    $query = new WP_Query(array("tag" => 'topic-' . $topic, "orderby" => 'date', "order" => 'ASC'));
    if ($query->posts) {
        while ($query->have_posts()) : $query->the_post();
            $a[$post->ID] = $post;
        endwhile;
    }
    $keys = array_keys($a);
    if (array_search($ID, $keys)) {
        $amount = count($a);
        $query = new WP_Query(array("tag" => 'topic-' . $topic . '-main'));
        if ($query->posts) {
            while ($query->have_posts()) : $query->the_post();
                $topicPin = $post;
            endwhile;
        }
    } else $topic = false;
}
if (!$topic) {
    $query = new WP_Query(array("post_status" => 'publish', "orderby" => 'date', "order" => 'ASC'));
    if ($query->posts) {
        while ($query->have_posts()) : $query->the_post();
            $a[$post->ID] = $post;
        endwhile;
    }
    $amount = count($a);
}

if ($amount > 2) {
    $keys = array_keys($a);
    $next = $keys[(array_search($ID, $keys) + 1) % count($keys)];
    $prev = $keys[(array_search($ID, $keys) - 1) % count($keys)];
    echo viewPostNav($topicPin, $a[$prev], $a[$next]);
}?>