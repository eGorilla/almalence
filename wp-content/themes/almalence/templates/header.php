<header class="header">
    <div class="container">
        <?=viewLogo();?>
        <?php get_search_form();?>
    </div>
    <div class="container menu">
        <?php get_template_part('templates/nav-main');?>
    </div>
</header>
