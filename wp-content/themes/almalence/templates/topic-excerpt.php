<?php
$post = $GLOBALS["post"];
$ID = get_the_ID();
$thumbnail = getPostExcerptThumbnail($ID);
?>
<article id="post-<?=$ID;?>" class="topic-excerpt">
    <div class="bg" style="<?=$thumbnail;?>"></div>
    <div class="container">
        <div class="inner">
            <figure class="thumbnail" style="<?=$thumbnail;?>"></figure>

            <header>
                <h1><?php echo $post->post_title;?></h1>
            </header>

            <div class="body">
                <?php the_excerpt();?>
            </div>

            <?=viewLinkViewMore($ID);?>
        </div>
    </div>
</article>

<script>
    let el = document.querySelector('.topic-excerpt'),
        cont = el.querySelector('.inner'),
        excerpt = el.querySelector('.body'), eh = excerpt.offsetHeight;
    let btnMore = el.querySelector('.a-view-more');
    let cl = 'topic-content';
    let p = el.querySelector('.' + cl), ph = 0;
    if (!p) {
        p = document.createElement('div');
        p.className = cl;
        el.parentNode.append(p);
    }

    function slide() {
        u.active(el);
        p.style["height"] = 0 + 'px';
        p.style["visibility"] = 'visible';
        cont.style["minHeight"] = 'auto';
        slideShow(p, ph, false, 100);
        slideHide(excerpt, eh, function () {
            u.scrollTo(el, -40);
        }, 100);
    }

    btnMore.addEventListener('click', function () {
        if (p.innerHTML === '') {
            u.wait(el);
            u.getJSON(URI_INC + 'load-post-content.php', {"p": btnMore.getAttribute('data-id')}, function (response) {
                p.style["visibility"] = 'hidden';
                p.style["overflow"] = 'hidden';
                p.innerHTML = '<div class="container"><figure class="thumbnail" style="<?=$thumbnail;?>"></figure><a class="a-view-less"></a>' + response + '</div>';

                setTimeout(function () {
                    let btnLess = p.querySelector('.a-view-less');
                    btnLess.addEventListener('click', function () {
                        u.active(el, false);
                        slideHide(p, p.offsetHeight, false, 100);
                        slideShow(excerpt, eh, function () {
                            cont.style["minHeight"] = '';
                            u.scrollTo(el, -40);
                        }, 100);
                    });

                    u.wait(el, false);
                    ph = p.offsetHeight;
                    slide();
                }, 100);
            });
        } else slide();
    });
</script>