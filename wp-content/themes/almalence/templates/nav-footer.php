<nav class="nav-footer">
    <?php
    wp_nav_menu(array(
        'theme_location'    => '',
        'container'         => false,
        'items_wrap'        => '<ul>%3$s</ul>'
    ));
    ?>
</nav>