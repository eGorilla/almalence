<?php
$post = $GLOBALS["post"];
$ID = get_the_ID();
$thumbnail = getPostExcerptThumbnail($ID);
?>
<article id="post-<?=$ID;?>" class="topic-excerpt-open">
    <div class="bg" style="<?=$thumbnail;?>"></div>
    <div class="container">
        <figure class="thumbnail" style="<?=$thumbnail;?>"></figure>

        <header>
            <h1><?php echo $post->post_title;?></h1>
        </header>

        <div class="body">
            <?php the_content();?>
        </div>
    </div>
</article>