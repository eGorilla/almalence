<?php
$ID = get_the_ID();
$tags = wp_get_post_tags($ID);
$link = setPostLink($tags);
?>
<article id="post-<?=$ID; ?>" class="new">
	<div class="body">
        <figure class="thumbnail" style="<?=viewPostArticleThumbnail($ID); ?>"></figure>
        <header>
            <h2><?php viewPostShare($ID); ?><?=$post->post_title;?></h2>
            <?php viewPostCategories(); ?>
            <?php viewPostTags($tags); ?>
        </header>

        <div class="post-excerpt">
            <?php the_excerpt();?>
        </div>
	</div>

    <div class="view-more" data-id="<?=$ID; ?>"><a class="a-view-more">READ FULL ARTICLE</a><a class="a-view-less">VIEW LESS</a><i class="i-waiting"></i></div>
</article>