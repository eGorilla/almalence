<?php
$tags = wp_get_post_tags(get_the_ID());
$link = setPostLink($tags);
?>
<article id="post-<?php the_ID(); ?>">
    <header>
        <a href="<?=$link;?>">
            <?php viewPostPinThumbnail(); ?>
            <h2><?=$post->post_title;?></h2>
        </a>
	</header>

	<div class="body">
        <?php viewPostCategories(); ?>
        <?php viewPostTags($tags); ?>
        <a href="<?=$link;?>" class="read-more"><?php the_excerpt();?></a>
	</div>
</article>