<?php
$link = $post->post_name;
$ID = get_the_ID();
$thumbnail = getPostExcerptThumbnail($ID);
?>
<li>
    <div class="inner">
        <a href="<?=$link;?>" class="a-view-more">read post</a>

        <figure class="thumbnail" style="<?=$thumbnail;?>"></figure>

        <header>
            <h2><?php echo $post->post_title;?></h2>
        </header>

        <div class="body">
            <?php the_excerpt();?>
        </div>
    </div>
</li>