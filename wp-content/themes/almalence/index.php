<?php
$args = getArgs();
$error = false;
$tpl = 'main-page';

if ($args) {
	if (isset($args["t"])) {
	    $tags = get_tags(["name__like" => TOPIC_PFX . $args["t"]]);
        $tags ? $tpl = TOPIC_TITLE : $error = true;
    }
	elseif (isset($args["categories"])) $tpl = 'categories';
    elseif (isset($args["page"])) $tpl = 'page';
    elseif (isset($args["sitemap"])) $tpl = 'sitemap';
    elseif (isset($args["tags"])) $tpl = 'tags';
    else $error = true;
}

if ($error) $tpl = '404';

include getInc($tpl);

get_footer();