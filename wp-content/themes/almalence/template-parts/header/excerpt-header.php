<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Almalence
 * @since Almalence 1.0
 */

// Don't show the title if the post-format is `aside` or `status`.
$post_format = get_post_format();
if ( 'aside' === $post_format || 'status' === $post_format ) {
	return;
}
?>

<header>
	<?php
	the_title( sprintf( '<h2><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' );
	viewPostPinThumbnail();
	?>
</header>
