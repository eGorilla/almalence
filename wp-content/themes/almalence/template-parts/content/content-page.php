<?php
get_header(null, array('title' => $post->post_title));
$ID = get_the_ID();
$tags = wp_get_post_tags($ID);
?>
<div class="container">
    <article id="post-<?=$ID;?>" class="single">
        <header>
            <h1><?php viewPostShare($ID); ?><?=$post->post_title;?></h1>
        </header>

        <div class="body">
            <?=$post->post_content;?>
        </div>
    </article>
</div>
<?php get_footer();?>
