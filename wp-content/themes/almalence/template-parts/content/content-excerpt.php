<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Almalence
 * @since Almalence 1.0
 */

?>

<article id="post-<?php the_ID(); ?>">
	<?php get_template_part( 'template-parts/header/excerpt-header', get_post_format() ); ?>

	<div class="body">
		<?php get_template_part( 'template-parts/excerpt/excerpt', get_post_format() ); ?>
	</div>

	<footer>
		<?php almalence_entry_meta_footer(); ?>
	</footer>
</article>