<?php
/**
 * Customize API: Almalence_Customize_Notice_Control class
 *
 * @package WordPress
 * @subpackage Almalence
 * @since Almalence 1.0
 */

/**
 * Customize Notice Control class.
 *
 * @since Almalence 1.0
 *
 * @see WP_Customize_Control
 */
class Almalence_Customize_Notice_Control extends WP_Customize_Control {
	/**
	 * The control type.
	 *
	 * @since Almalence 1.0
	 *
	 * @var string
	 */
	public $type = 'twenty-twenty-one-notice';

	/**
	 * Renders the control content.
	 *
	 * This simply prints the notice we need.
	 *
	 * @access public
	 *
	 * @since Almalence 1.0
	 *
	 * @return void
	 */
	public function render_content() {
		?>
		<div class="notice notice-warning">
			<p><?php esc_html_e( 'To access the Dark Mode settings, select a light background color.', 'almalence' ); ?></p>
			<p><a href="<?php echo esc_url( __( 'https://wordpress.org/support/article/twenty-twenty-one/#dark-mode-support', 'almalence' ) ); ?>">
				<?php esc_html_e( 'Learn more about Dark Mode.', 'almalence' ); ?>
			</a></p>
		</div>
		<?php
	}
}
