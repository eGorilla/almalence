const body = document.querySelector('body'),
    wrapper = body.querySelector('.wrapper'),
    main = body.querySelector('main'),
    header = body.querySelector('header.header'),
    navMain = body.querySelector('.nav-main'),
    navSearch = body.querySelector('.b-search');
const storageURL = '/wp-content/uploads/';
const gap = -10;

let navMainInit = false, navMainScrollStatus = false, navMainWidth = u.viewport().w;

function watchScroll() {
    u.watchScroll();
    u.watchScrollDir();
    u.active(navMain, false);
    u.active(navSearch, false);
}

function setNavMain() {
    const cStatus = body.classList.contains('scrolled'), cWidth = u.viewport().w;
    if (navMainInit && cStatus === navMainScrollStatus && cWidth === navMainWidth) return;

    navMainInit = true;
    navMainWidth = cWidth;
    navMainScrollStatus = cStatus;

    const ulMain = navMain.querySelector('.ul-main'),
        ul = navMain.querySelector('.ul-helper'),
        ulDummy = navMain.querySelector('.ul-dummy'),
        toggle = navMain.querySelector('.a-bars'),
        toggleW = toggle.offsetWidth;
    toggle.onclick = function () {
        u.active(navSearch, false);
        u.active(navMain, 'toggle');
    };
    let cs = getComputedStyle(navMain, null), width = parseInt(cs.getPropertyValue('width')) - parseInt(cs.getPropertyValue('padding-left')) - parseInt(cs.getPropertyValue('padding-right')) - 35;
    let list = navMain.querySelectorAll('li'), total = list.length;

    u.active(toggle, false);

    ulMain.innerHTML = '';
    ul.innerHTML = '';
    ulDummy.innerHTML = '';

    for (let n = 0; n < total; n++) {
        let el = list[n];
        ulDummy.append(el);
    }

    if (ulDummy.offsetWidth > width) {
        let w = 0, a = [], a2 = [];
        for (let n = 0; n < total; n++) {
            let el = list[n];
            w += el.offsetWidth;
            let cw = toggleW *2;
            w + cw < width ? a.push(el) : a2.push(el);
        }
        for (let n = 0; n < a.length; n++) {
            let el = a[n];
            ulMain.append(el);
        }
        for (let n = 0; n < a2.length; n++) {
            let el = a2[n];
            ul.append(el);
        }
        u.active(toggle);
    } else {
        for (let n = 0; n < list.length; n++) {
            let el = list[n];
            ulMain.append(el);
        }
    }
}

function initGallery(el, func) {
    if (el === undefined) el = document;
    else if (typeof el === 'string') el = document.querySelector(el);
    let a = el.querySelectorAll('gallery');
    if (a.length) {
        let counter = galeries.length;
        a.forEach(function (el) {
            galeries[counter] = new CGallery();
            galeries[counter].init({
                "dummy": el,
                "url": storageURL,
                "callbackInit": function () {
                    if (func !== undefined) func();
                }
            });
            counter++;
        });
    } else if (func !== undefined) func();
}

let gls = 0;
function resizeGallery() {
    if (galeries.length) {
        clearTimeout(gls);
        gls = setTimeout(function () {
            galeries.forEach(function (el) {
                el.setSize();

                if (el["type"] === 'slider') el.setSlider();
            });
        }, 300);
    }
}

function getArticleContent(a) {
    if (a === undefined) a = document.querySelectorAll('.list-articles article');
    a.forEach(function (el) {
        el.classList.remove('new');
        let excerpt = el.querySelector('.post-excerpt'), eh = excerpt.offsetHeight, c = el.querySelector('.view-more');
        let btnMore = el.querySelector('.a-view-more'), btnLess = el.querySelector('.a-view-less');
        let cl = 'post-content';
        let p = el.querySelector('.' + cl), ph = 0;
        if (!p) {
            p = document.createElement('div');
            p.className = cl;
            el.insertBefore(p, c);
        }

        function scroll() {
            u.scrollTo(el, -60);
        }

        function slide() {
            p.style["height"] = 0 + 'px';
            p.style["visibility"] = 'visible';
            slideShow(p, ph);
            slideHide(excerpt, eh, function () {
                u.active(el);
                scroll();
            });
        }

        btnMore.addEventListener('click', function () {
            if (p.innerHTML === '') {
                u.wait(el);
                u.getJSON(URI_INC + 'load-post-content.php', {"p": c.getAttribute('data-id')}, function (response) {
                    p.style["visibility"] = 'hidden';
                    p.innerHTML = response;
                    initGallery(p, function () {
                        ph = c.offsetTop - p.offsetTop + c.offsetHeight;
                        u.wait(el, false);
                        slide();
                    });
                });
            } else slide();
        });

        btnLess.addEventListener('click', function () {
            u.active(el, false);
            slideHide(p, p.offsetHeight);
            slideShow(excerpt, eh, scroll);
        });
    });
}

let loadPostWM = [];
function initLoadPostWM(c) {
    c = c || document;
    const a = c.querySelectorAll('.watch-mark');
    a.forEach(function (el) {
        loadPostWM.push(el);
    });
}
function watchLoadPostWM() {
    const total = loadPostWM.length;
    if (total) {
        for (let n = 0; n < total; n++) {
            let el = loadPostWM[n];
            if (u.checkVisible(el)) {
                let func = el.getAttribute('data-func');
                func ? window[func](el) : el.remove();
                loadPostWM.splice(n, 1);
                break;
            }
        }
    }
}

function loadPosts(el) {
    const a = JSON.parse(el.getAttribute('data-attr'));
    u.wait(el);
    u.getJSON(URI_INC + 'load-posts.php', {"obj": JSON.stringify(a)}, function (response) {
        if (response) {
            const c = document.querySelector('.list-articles');
            document.getElementById('post-' + a["ID"]).insertAdjacentHTML('afterend', response);
            el.remove();
            getArticleContent(c.querySelectorAll('.new'));
            initLoadPostWM(c);
        }
    }, 'text');
}

function initShare() {
    let a = document.querySelectorAll('.post-share a');
    a.forEach(function (el) {
        el.onclick = function () {
            let id = el.getAttribute('data-id'), data = el.getAttribute('data-data');
            share(id, data);
        };
    })
}

function initNavSearch() {
    document.querySelector('.a-search').onclick = function () {
        u.active(navMain, false);
        u.active(navSearch, 'toggle');
    };
}

PLA.push(setVP, watchScroll, setToggle, initGallery, setNavMain, getArticleContent, initLoadPostWM, initShare, initNavSearch);
WLA.push({"resize": setVP}, {"scroll": watchScroll}, {"scroll": watchLoadPostWM}, {"resize": resizeGallery}, {"resize": setNavMain}, {"scroll": setNavMain});