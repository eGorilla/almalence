let PL, PLA = [], WLA = [], galeries = [];

const screenDelay = 500;
const slideTimeout = 10;
const slideStep = 10;

if (typeof Object.assign != 'function') {
    Object.assign = function(target) {
        'use strict';
        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        target = Object(target);
        for (let index = 1; index < arguments.length; index++) {
            let source = arguments[index];
            if (source != null) {
                for (let key in source) {
                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                        target[key] = source[key];
                    }
                }
            }
        }
        return target;
    };
}

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

(function() {
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.matchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector;

    }
})();

(function() {
    if (!Element.prototype.closest) {
        Element.prototype.closest = function(css) {
            let node = this;
            while (node) {
                if (node.matches(css)) return node;
                else node = node.parentElement;
            }
            return null;
        };
    }
})();

(function (arr) {
    arr.forEach(function (item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

if (!Object.getOwnPropertyDescriptor(Element.prototype,'classList')){
    if (HTMLElement&&Object.getOwnPropertyDescriptor(HTMLElement.prototype,'classList')){
        Object.defineProperty(Element.prototype,'classList',Object.getOwnPropertyDescriptor(HTMLElement.prototype,'classList'));
    }
}

class CUtils {
    constructor() {
        const params = {
            vp: ['xs','sm','md','lg','mx'],
            size: {
                xs: 320,
                sm: 480,
                middle: 640,
                md: 768,
                lg: 960,
                mx: 1280
            },
            cScroll: 0,
            scrollDir: true
        };
        Object.assign(this, params);
    }

    active(el, v) {
        const cl = 'active',
            cList = el.classList;
        let list, total;
        if (v === undefined) v = true;
        switch (v) {
            case true:
                cList.add(cl);
                break;
            case false:
                cList.remove(cl);
                break;
            case 'clean':
                el.querySelectorAll('.' + cl).forEach(function (el) {
                    u.active(el, false);
                });
                break;
            case 'is':
                return cList.contains(cl);
            case 'one':
                list = el.parentNode.children;
                total = list.length;
                for (let n = 0; n < total; n++) u.active(list[n], false);
                u.active(el);
                break;
            case 'toggle':
                const active = u.active(el, 'is');
                cList.toggle(cl);
                return !active;
        }
    }

    arrayDiff(a, b) {
        if (typeof a === 'string') a = [a];
        if (typeof b === 'string') b = [b];
        return a.filter(function(elm) {
            return b.indexOf(elm) === -1;
        })
    }

    checkVisible(el, h, mode) {
        h = h || 0;
        mode = mode || 'visible';

        const rect = el.getBoundingClientRect();
        const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
        const above = rect.bottom - h < 0;
        const below = rect.top - viewHeight + h >= 0;

        return mode === 'above' ? above : (mode === 'below' ? below : !above && !below);
    }

    obj2URL(obj) {
        let str = '';
        for (let key in obj) {
            if (str !== '') str += '&';
            str += key + '=' + encodeURIComponent(obj[key]);
        }
        return str;
    }

    getJSON(url, data, callback, type) {
        const xhr = new XMLHttpRequest();
        if (data) url += '?' + u.obj2URL(data);
        xhr.open('GET', url, true);
        xhr.setRequestHeader("X-Requested-With", 'XMLHttpRequest');
        xhr.responseType = type || 'json';
        xhr.onload = function () {
            if (xhr.status === 200) if (callback) callback(xhr.response);
        };
        xhr.send();
    }

    postJSON(url, data, callback) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(u.obj2URL(data));
        xhr.onloadend = function () {
            if (xhr.status === 200) {
                if (callback) callback(xhr.responseText);
            }
        };
    }

    clearString(str) {
        return str.replace(/(\r\n|\n|\r|\t)/gm, '');
    }

    scrollTo(e, gap) {
        if (typeof e === 'string') e = document.querySelector(e);
        if (gap === undefined) gap = 0;
        const box = e.getBoundingClientRect();
        let top = Number(box.top + pageYOffset + gap);
        if (top < 0) top = 0;
        window.scrollTo({
            "top": top,
            "behavior": 'smooth'
        });
    }

    viewport(doc) {
        doc = doc || document;
        let el = doc.compatMode === 'CSS1Compat' ? doc.documentElement : doc.body,
            w = el.clientWidth,
            h = el.clientHeight,
            cols = 0,
            vp = 'xs',
            sizes = u.size;
        if (w < sizes.sm) {
            cols = 1;
            vp = 'xs';
        }
        if (w >= sizes.sm) {
            cols = 1;
            vp = 'sm';
        }
        if (w >= sizes.md) {
            cols = 2;
            vp = 'md';
        }
        if (w >= sizes.lg) {
            cols = 3;
            vp = 'lg';
        }
        if (w >= sizes.mx) {
            cols = 4;
            vp = 'mx';
        }
        return {w: w, h: h, cols: cols, vp: vp};
    }

    wait(el, v) {
        const cl = 'waiting',
            cList = el.classList;
        if (v === undefined) v = true;
        switch (v) {
            case true:
                cList.add(cl);
                break;
            case false:
                cList.remove(cl);
                break;
        }
    }

    watchScroll(v) {
        if (v === undefined) v = 0;
        const cl = 'scrolled';
        const e = body.classList;
        const scrolled = (window.pageYOffset || document.documentElement.scrollTop) > v;
        scrolled ? e.add(cl) : e.remove(cl);
        return scrolled;
    }

    watchScrollDir() {
        const st = window.pageYOffset || document.documentElement.scrollTop;
        this.scrollDir = st > this.cScroll;
        this.cScroll = st <= 0 ? 0 : st;
    };
}
const u = new CUtils();

class CPreLoad {
    constructor() {
        const params = {
            "insert": true,
            "attr-img": 'data-preload-img',
            "spinner-el": '<span class="i-spinner"></span>',
            "spinner-cont": 'loader'
        };
        Object.assign(this, params);
    }

    spinner(v, wrap) {
        if (v === undefined) v = true;
        if (wrap === undefined) wrap = true;
        if (v) return wrap ? '<div class="' + this["spinner-cont"] + '">' + this["spinner-el"] + '</div>' : this["spinner-el"];
        else {
            this["loader"].remove();
            delete this["loader"];
        }
    }

    loadImage(type, src, callback) {
        const self = this;
        const img = new Image();
        img.onload = function () {
            self.insertImage(type, this, src);
            if (callback) callback();
        };
        img.src = src;
    }

    insertImage(type, img, src) {
        const el = document.querySelector('[' + type + '="' + src + '"]');
        if (this["insert"]) {
            img.className = this.parseStyle(el);
            el.parentNode.replaceChild(img, el);
        }
        else {
            el.innerHTML = this.renderImage(type, src);
            el.removeAttribute(type);
        }
    }

    renderImage(type, src) {
        let str = '';
        switch (type) {
            case this["attr-bg"]:
                str += '<figure style="background-image: url(\'' + src + '\');"></figure>';
                break;
            case this["attr-img"]:
                str += '<img src="' + src + '" alt="" />';
                break;
            case this["attr-video"]:
                str += '<iframe width="' + this["video-width"] + '" height="' + this["video-height"] + '" src="' + src + '" frameborder="0" allowfullscreen></iframe>';
                break;
        }
        return str;
    }

    parseStyle(el) {
        const cList = el.classList;
        let str = '';
        if (cList.length) {
            let o = [];
            for (let i in cList) {
                if (/^[-]?\d+$/.test(i)) o.push(cList[i]);
            }
            str = o.join(' ');
        }
        return str;
    }
}

class CPreLoadMedia extends CPreLoad{
    init(el, start) {
        this["el"] = el;
        if (start !== undefined) {
            const spinnerCont = document.createElement('div');
            spinnerCont.className = this["spinner-cont"];
            spinnerCont.innerHTML = this.spinner(true, false);
            body.insertBefore(spinnerCont, this["el"]);
            this["loader"] = spinnerCont;
        } else {
            this.parseDummies();
            this.loadImages();
        }
    }

    parseDummies() {
        const da = this["attr-img"];
        let a = [];
        const list = document.querySelectorAll('[' + da + ']');
        list.forEach (function (el){
            let src = el.getAttribute(da);
            a.push(src);
        });
        this["dummies"] = a;
    }

    loadImages(a) {
        if (!a) a = this["dummies"];
        const src = a.shift();
        if (src) {
            const self = this;
            this.loadImage(self["attr-img"], src, function () {
                a ? self.loadImages(a) : self.setAfter();
            });
        } else this.setAfter();
    }

    deleteStorage() {
        const storage = document.querySelector('.images-storage');
        if (storage) storage.remove();
    }

    setAfter() {
        this.deleteStorage();
        this.spinner(false);
        u.active(this["el"]);
        if (this.callback) this.callback();
    }
}

class CSlider {
    constructor() {
        let params = {
            "el": {},
            "list-class-name": '.list',
            "list": {},
            "total": 0,
            "locked": false,
            "nav": true,
            "nav-obj": ['prev', 'next'],
            "duration": 500,
            "cid": 0,
            "sls": 0,
            "show": false,
            "show-delay": 1500,
            "show-dir": 'next',
            "show-watch": true,
            "initCallback": false,
            "navClickCallback": false,
            "slideCallback": false
        };
        Object.assign(this, params);
    }

    init(params) {
        if(params) Object.assign(this, params);
        this.create();
    }

    create() {
        if (this.nav) this.createNav();
        this.initList();
        this.activity();

        if (this["show"]) this.initShow(this);
    }

    initList() {
        this["list"] = this["el"].querySelector(this["list-class-name"]);
        let a = Array.from(this["list"].childNodes);
        this["total"] = a.length;

        if (this.initListHelper) this.initListHelper(a);
        let first = a[this["cid"]];

        if (this.initCallback) this.initCallback(a);

        if (this.slideCallback) this.slideCallback(first);
        u.active(first, 'one');

        if (this.loadCallback) this.loadCallback();
    }

    initListHelper(a) {
        a.forEach(function (el) {
            if (!el.hasAttribute('data-id')) el.setAttribute('data-id', a.indexOf(el));
        });
    }

    createNav() {
        const btns = this.el.querySelectorAll('.next, .prev');
        btns.forEach(function (e) {
            e.remove();
        });
        let self = this;
        this["nav-obj"].forEach(function (v) {
            let arrow = document.createElement('span');
            let link = document.createElement('a');
            arrow.className = v;
            link.onclick = function () {
                if (!self["locked"]) self.slide(v);
            };
            arrow.appendChild(link);
            self["el"].appendChild(arrow);
        });
    }

    slide(dir, func) {
        let self = this;
        let list = this["list"];
        let next = dir === 'next';

        let cid = next ? this["cid"] + 1 : this["cid"] - 1;
        if (cid < 0) cid = this["total"] -1;
        if (cid > this["total"] -1) cid = 0;
        this["cid"] = cid;

        this.lockSlide(true);

        let e = next ? list.firstChild : list.lastChild;
        e.classList.add('moved');

        let el = next ? e.nextSibling : e;
        if (this.checkItem) this.checkItem(el);

        if (this.navClickCallback) this.navClickCallback(dir);

        if (next) {
            setTimeout(function () {
                list.appendChild(e);
                self.setSlide(e, el, func, dir);
            }, self["duration"]);
        } else {
            list.insertBefore(e, list.firstChild);
            setTimeout(function () {
                self.setSlide(e, el, func, dir);
            }, 10);
        }
    }

    lockSlide(v) {
        const cl = 'locked', cll = this["el"].parentNode.parentNode.classList;
        v ? cll.add(cl) : cll.remove(cl);
        this[cl] = v;
    }

    setSlide(e, el, func, dir) {
        const self = this;
        if (dir === 'next') {
            e.classList.remove('moved');
            u.active(el, 'one');
            if (this.slideCallback) this.slideCallback(el, dir);
            if (func) func();
            this.lockSlide(false);
        } else {
            e.classList.remove('moved');
            u.active(el, 'one');
            if (this.slideCallback) this.slideCallback(el, dir);
            setTimeout(function () {
                if (func) func();
                self.lockSlide(false);
            }, self["duration"]);
        }
    }

    initShow(self) {
        this.stopShow();
        this.startShow(self);
    }

    startShow(self) {
        this["sls"] = setTimeout(function(){
            self.slide(self["show-dir"], self.initShow(self));
        }, this["show-delay"]);
    }

    stopShow() {
        let ls = this["sls"];
        if (ls) clearTimeout(ls);
    }

    activity() {
        let self = this;

        if (this["show"] && this["show-watch"]) {
            const el = this["show-watcher"] || this["el"];
            el.onmouseover = function() {
                self.stopShow();
            };
            el.onmouseout = function() {
                self.initShow(self);
            };
        }
    }
}

class CSliderMedia extends  CSlider {
    constructor(name) {
        super(name);
        const params = {
            "el": '',
            "src": '',
            "insert": false,
            "attr-media": 'data-media',
            "attr-bg": 'data-bg',
            "attr-img": 'data-img',
            "attr-collection": 'data-collection',
            "attr-video": 'data-video',
            "video-width": 560,
            "video-height": 315,
            "collection-slider": 3,
            "collectionCallback": false
        };
        Object.assign(this, params);
    }

    init(params) {
        if(params) Object.assign(this, params);
        this.create();
    }

    checkItem(el) {
        const self = this;
        const type = this["attr-media"];
        const dm = el.getAttribute(type);
        if (dm !== null) {
            this["el"] = el;
            el.removeAttribute(type);
            const collection = el.querySelector('[' + this["attr-collection"] + ']');
            if (collection) this.setCollection(collection);

            const obj = el.querySelectorAll('[' + this["attr-bg"] + '], [' + this["attr-img"] + '], [' + this["attr-video"] + ']');
            let a = [];
            obj.forEach(function(el){
                el.innerHTML= self.spinner();
                a.push(el);
            });
            if (a.length) this.checkItemHelper(a);
        }
    }

    checkItemHelper(a) {
        const self = this;
        const el = a.shift();
        if (el) {
            let type = this["attr-img"];
            let src = el.getAttribute(type);
            if (!src) {
                type = this["attr-bg"];
                src = el.getAttribute(type);
            }
            if (src) {
                this.loadImage(type, src, function () {
                    self.checkItemHelper(a);
                }, el);
            } else {
                type = this["attr-video"];
                src = el.getAttribute(type);
                if (src) {
                    el.innerHTML = this.renderImage(type, src);
                    this.setAfter();
                }
            }
        } else this.setAfter();
    }

    setAfter() {}

    renderCollectionMain(el, a) {
        let src = a[0];
        this["src"] = src;
        el.className = 'img-main';
        el.setAttribute(this["attr-bg"], src);
    }

    renderCollectionNav(el, a) {
        const self = this;
        let c = document.createElement('div');
        c.className = 'slider';
        let nav = document.createElement('ul');
        nav.className = 'list';
        let total = this["total"] = a.length;
        for (let n = 0; n < total; n++) {
            let li = document.createElement('li');
            li.setAttribute(this["attr-img"], a[n]);
            li.addEventListener('click', function () {
                self.collectionCallback(this);
            });
            nav.appendChild(li);
        }
        c.appendChild(nav);

        const slider = this["collection-slider"],
            duration = this["collection-slider-duration"];
        if (slider && slider < this["total"]) {
            const cs = new CSlider();
            cs.init({
                    "height": "100%",
                    "duration": duration,
                    "el": c,
                    "collection-slider": slider
                }
            );
        }
        return c;
    }

    setCollection(el) {
        const p = el.parentNode;
        const pa = this["attr-collection"];
        const a = el.getAttribute(pa).split(',');
        el.removeAttribute(pa);

        this.renderCollectionMain(el, a);

        const nav = this.renderCollectionNav(el, a);
        p.appendChild(nav);
    }

    setEffect(c, src, a) {
        const self = this;
        if (typeof a != 'object') a = [a];
        const total = a.length;
        if (total) {
            for (let n = 0; n < total; n++) {
                let e, clone, img;
                let effect = a[n];
                switch (effect) {
                    case 'bg':
                        e = c.querySelector('.img-bg');
                        if (!e) {
                            e = c.querySelector('.img-main');
                            clone = e.cloneNode(true);
                            clone.className = 'img-bg';
                            c.insertBefore(clone, e);
                            e = c.querySelector('.img-bg');
                        }
                        img = e.querySelector('figure');
                        this.setEffect(img, src, 'img');
                        break;
                    case 'img':
                        c.style.backgroundImage = 'url("' + src + '")';
                        break;
                    case 'tunnel':
                        e = c.querySelector('.img-main');
                        img = e.querySelector('figure');
                        clone = c.querySelectorAll('.' + effect);
                        if (clone.length) for (let n = 0; n < clone.length; n++) clone[n].remove();
                        for (let n = 0; n < 3; n++) {
                            clone = img.cloneNode(true);
                            e.appendChild(clone);
                            clone.className = effect + ' layer-' + (n + 1);
                        }
                        setTimeout(function () {
                            self.setEffect(img, src, 'img');
                        },400);
                        break;
                    default :
                        e = c.querySelector('.img-main');
                        img = e.querySelector('figure');
                        clone = c.querySelector('.' + effect);
                        if (clone) clone.remove();
                        clone = img.cloneNode(true);
                        e.appendChild(clone);
                        clone.className = effect;
                        this.setEffect(img, src, 'img');
                        setTimeout(function () {
                            clone.remove();
                        }, self["duration"]);
                        break;
                }
            }
        }
    }
}

class CCalendar {
    constructor() {
        const params = {
            "id": 'calendar',
            "el": false,
            "nav": [],
            "table": '',
            "monthNames": ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
            "dayNames": ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            "divider": '-',
            "input": '',
            "Event": false,
            "date": [],
            "today": [],
            "toggle": '.date-picker-button',
            "callbackApply": false
        };
        Object.assign(this, params);
    }

    init(params) {
        if(params) Object.assign(this, params);

        if(typeof this["toggle"] === 'string') this["toggle"] = document.querySelector(this["toggle"]);

        const self = this;
        this["toggle"].addEventListener('click', function(e){
            self["Event"] = e;
            self.open();
        });

        this["input"] = document.querySelector(this["toggle"].getAttribute('data-id'));
    }

    create() {
        let div = this["el"] = document.createElement('div');
        div.id = this["id"];
        div.className = this["cl"] ? this["cl"] : this["id"];
        document.body.appendChild(div);

        let nav = document.createElement('nav');
        let b = this["nav"]["back"] = document.createElement('a');
        b.innerHTML = '<i class="i-chevron-left"></i>';
        nav.appendChild(b);
        let title = this["nav"]["title"] = document.createElement('span');
        nav.appendChild(title);
        let f = this["nav"]["forward"] = document.createElement('a');
        f.innerHTML = '<i class="i-chevron-right"></i>';
        nav.appendChild(f);
        let c = this["nav"]["close"] = document.createElement('a');
        c.innerHTML = '<i class="i-times-circle"></i>';
        nav.appendChild(c);
        this["el"].appendChild(nav);

        let table = this["table"] = document.createElement('table');
        this["el"].appendChild(table);
    }

    open() {
        const d = new Date();
        this["date"] = [d.getFullYear(), d.getMonth(), d.getDate()];
        Object.assign(this["today"], this["date"]);

        this.render();
    }

    close() {
        this["el"].remove();
    }

    render() {
        if (this["el"]) this.close();
        const date = this["date"];
        let d = new Date();
        let year = (date[0] == null) ? d.getFullYear() : Number(date[0]);
        let month = (date[1] == null) ? d.getMonth() : Number(date[1]);
        while (month < 0) {
            month += 12;
            year--;
        }
        while (month >= 12) {
            month -= 12;
            year++;
        }
        let day = (date[2] == null) ? 0 : Number(date[2]);
        d = new Date(year, month, 1);
        let nextMonth = new Date(year, month + 1, 1);
        let weekStart = d.getDay();
        if (weekStart === 0) weekStart = 7;
        let days = Math.round((nextMonth.getTime() - d.getTime()) / 86400000) + 1;

        let str = '';
        str += '<thead><tr>';
        this["dayNames"].forEach(function (e) {
            str += '<th>' + e + '</th>';
        });
        str += '</tr></thead>';

        let counter = 0;
        str += '<tbody>';
        let tail = weekStart - 1;
        if (tail) {
            str += '<tr>';
            for (let n = 0; n < tail; n++) {
                counter++;
                str += '<td></td>';
            }
        }
        for (let n = 1; n < days; n++) {
            counter++;
            let dif = counter % 7;
            if (dif === 1) str += '<tr>';
            let today = (n === this["today"][2] && month === this["today"][1] && year === this["today"][0]) ? ' class="today"' : '';
            str += '<td' + today + '><span>' + n + '</span></td>';
            if (n === days - 1) {
                tail = 7 - dif;
                if (dif && tail) {
                    for (let n = 0; n < tail; n++) str += '<td></td>';
                    str += '</tr>';
                    break;
                }
            }
            if (dif === 0) str += '</tr>';
        }
        str += '</tbody>';

        this["date"] = [year, month, day];

        this.create();
        this.activity();
        this.setPosition();

        this["nav"]["title"].innerText = this["monthNames"][month] + ' ' + year;
        this["table"].innerHTML = str;
    }

    setDate(number) {
        return (number < 10) ? '0' + number : number;
    }

    setPosition() {
        let x = 0, y = 0;
        let e = this["Event"];
        if (e.pageX || e.pageY) {
            x = e.pageX;
            y = e.pageY;
        }
        else if (e.clientX || e.clientY) {
            x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        this["el"].style.left = x + 'px';
        this["el"].style.top = y + 'px';
        u.active(this["el"], true);
    }

    insertDate() {
        this["input"].value = this.setDate(this["date"][2]) + this.divider + this.setDate(this["date"][1] + 1) + this.divider + this.setDate(this["date"][0]);
        this.close();
        if (this.callbackApply) this.callbackApply();
    }

    activity() {
        const self = this;

        this["nav"]["back"].onclick = function () {
            self["date"][1] -= 1;
            self.render();
        };
        this["nav"]["forward"].onclick = function () {
            self["date"][1] += 1;
            self.render();
        };
        this["nav"]["close"].onclick = function () {
            self.close();
        };

        this["table"].onclick = function (e) {
            const el = e.target;
            if (el.tagName.toLowerCase() === 'td' || el.tagName.toLowerCase() === 'span') {
                const v = el.innerHTML;
                self["date"][2] = Number(v);
                self.insertDate();
            }
        };
    }
}

class CActiveForm {
    constructor() {
        const params = {
            "el": '',
            "selector": false,
            "nav": false,
            "cl": 'AF',
            "forms": {},
            "default": {},
            "changed": {},
            "required": {},
            "invalid": {},
            "data": {},
            "passGenLimits": {
                "uppercase": [1],
                "-": false,
                "numbers": [6]
            },
            "callbackApply": false,
            "callbackInit": false,
            "callbackReset": false
        };
        Object.assign(this, params);
    }

    init(params) {
        if (typeof params != 'object') this["selector"] = params;
        else Object.assign(this, params);

        if (this["selector"]) {
            this["el"] = document.querySelector(this["selector"]);

            this.createNav();
            this.initForms();
            this.activity();

            if (this.callbackInit) this.callbackInit();
        }
    }

    initForms() {
        const self = this;
        let forms = this["el"].querySelectorAll('form'), a = {};
        forms.forEach(function (e) {
            let id = e.getAttribute('data-id');
            a[id] = e;
            e.classList.add(self["cl"]);
        });
        this["forms"] = a;
        this.fillDefault();
    }

    fillDefault() {
        let def = this["default"], req = this["required"];
        let a = this["forms"];
        for (let id in a) {
            let els = a[id].querySelectorAll('input, select, textarea');
            let d = {}, r = {};
            els.forEach(function (el) {
                let name = el.name, v = el.value;
                if (el.closest('.required')) r[name] = true;
                switch (el.tagName.toLowerCase()) {
                    case 'input':
                        d[name] = el.type === 'checkbox' ? el.checked ? 'on' : 'off' : v;
                        break;
                    case 'select':
                        d[name] = el.selectedIndex ? [el.selectedIndex, v] : 0;
                        break;
                    case 'textarea':
                        d[name] = v;
                        break;
                }
            });
            def[id] = d;
            if (Object.keys(r).length) req[id] = r;
        }
    }

    resetDefault() {
        let a = this["changed"];
        for (let id in a) this.setDefault(this["forms"][id], this["default"][id]);
        this["changed"] = {};
        this["invalid"] = {};
        this.setActive();
    }

    setDefault(form, a) {
        let els = form.querySelectorAll('input, select, textarea');
        els.forEach(function (el) {
            let name = el.name;
            if (a[name] !== undefined) {
                switch (el.tagName.toLowerCase()) {
                    case 'input':
                        if (el.type === 'checkbox') el.checked = a[name] === 'on';
                        else if (el.type !== 'file') el.value = a[name];
                        break;
                    case 'select':
                        el.selectedIndex = a[name][0] ? a[name][0] : 0;
                        break;
                    case 'textarea':
                        el.value = a[name];
                        break;
                }
            }
        });
        this.setInvalids(false);
    }

    applyChanges() {
        this.initForms();
        this["changed"] = {};

        const cl = 'added';
        const el = this["el"].querySelector('.' + cl);
        if (el) {
            const id = el.getAttribute('data-id');
            const pic = this["default"][id]["user-pic"];
            setTimeout(function () {
                if (pic !== undefined) el.querySelector('.image-upload figure').style.backgroundImage = 'url("/profiles/images/' + pic + '")';
                el.classList.remove(cl);
            }, 100);
        }
    }

    checkChanges() {
        if (!Object.keys(this["changed"]).length) return false;
        for (let e in this["changed"]) {
            let required = this["required"][e];
            let a = [];
            for (let el in required) {
                let def = this["default"][e][el], chg = this["changed"][e][el];
                if (!def && chg === undefined) a[el] = true;
                else if (def && chg === '') a[el] = true;
                else if (typeof chg == 'object' && !chg[0]) a[el] = true;
                else if (def === 'off' && !chg) a[el] = true;
            }
            if (Object.keys(a).length) this["invalid"][e] = a;
        }
        return true;
    }

    fillChanges(el, v) {
        const id = el.closest('form').getAttribute('data-id'), name = el.name;
        const a = this["changed"][id];
        if (v === undefined) v = el.value;
        if (this["default"][id][name].toString() !== v.toString()) {
            if (!a) this["changed"][id] = {};
            this["changed"][id][name] = v;
        } else {
            if (a) {
                if (a[name]) {
                    delete a[name];
                    if (!Object.keys(a).length) delete this["changed"][id];
                }
            }
        }
        this.setActive(id);
    }

    resetChanges() {
        this.resetDefault();
        if (this.callbackReset) this.callbackReset();
    }

    setChanges() {
        let a = [];
        for (let id in this["changed"]) {
            let e = this["changed"][id];
            let b = {"ID": id};
            for (let el in e) {
                if (typeof e[el] == 'string') {
                    b[el] = this.escapeMsg ? this.escapeMsg(e[el]) : e[el];
                }
                else b[el] = e[el][1];
                if (el === 'avatar') b[el] = id + '-' + b[el];
            }
            b = this.checkHidden(id, b);
            a.push(b);
        }
        return a;
    }

    checkHidden(id, a) {
        const inputs = this["forms"][id].querySelectorAll('input[type=hidden]');
        if (inputs.length) {
            inputs.forEach(function (el) {
                a[el.name] = el.value;
            });
        }
        return a;
    }

    checkInvalid(el) {
        const p = el.closest('.invalid');
        if (p) {
            const id = el.closest('form').getAttribute('data-id');
            this.removeInvalid(p, id, el.name);
        }
    }

    removeInvalid(p, id, name) {
        const a = this["invalid"];
        if (a[id]) {
            if (a[id][name]) {
                delete a[id][name];
                if (!Object.keys(a[id]).length) delete a[id];
                p.classList.remove('invalid');
            }
            this.setActive(id);
        }
    }

    setInvalids(v) {
        if (v === undefined) v = true;
        const a = this["invalid"];
        if (v) {
            for (let id in a) {
                for (let e in a[id])  {
                    let el = this["forms"][id].querySelector('[name="' + e + '"]');
                    let p = el.closest('.required');
                    if (!p) p = el.closest('div');
                    p.classList.add('invalid');
                }
                this.setActive(id);
            }
        } else {
            for (let id in a) {
                for (let e in a[id])  {
                    let p = this["forms"][id].querySelector('.invalid');
                    p.classList.remove('invalid');
                }
                this.setActive(id);
            }
        }
    }

    createNav() {
        const cl = this["cl"] + '-nav';
        if (this["nav"]) {
            let nav = this["el"].querySelector('.' + cl);
            if (nav) nav.remove();
            nav = this["nav"] = document.createElement('nav');
            nav.className = cl;
            let resetButton = this["resetButton"] = document.createElement('button');
            resetButton.type = 'button';
            resetButton.className = 'reset';
            resetButton.innerHTML = 'Сбросить';
            nav.appendChild(resetButton);
            let submitButton = this["submitButton"] = document.createElement('button');
            submitButton.type = 'button';
            submitButton.className = 'save';
            submitButton.innerHTML = 'Сохранить';
            nav.appendChild(submitButton);
            this["el"].appendChild(nav);
        }
    }

    setActive(id) {
        const v = Object.keys(this["changed"]).length > 0 && !Object.keys(this["invalid"]).length > 0;
        this.setFormActive(id, v);
        this.setNavActive(v);
    }

    setFormActive(id, v) {
        if (id) u.active(this["forms"][id], v);
        else if (!v) {
            const a = this["forms"];
            for (let i in a) u.active(a[i], false);
        }
    }

    setNavActive(v) {
        if (this["nav"]) u.active(this["nav"], v);
    }

    apply() {
        if (this.checkChanges()) {
            if (Object.keys(this["invalid"]).length) this.setInvalids();
            else {
                const data = this.setChanges();
                this.applyHelper(data);
            }
        }
    }

    applyHelper(a, response) {
        const self = this;
        const data = a.shift();
        if (data) {
            let form = this["forms"][data["ID"]];
            this.setStatus(form, 'waiting');
            let URL = form["action"];
            if (form["enctype"] === 'multipart/form-data') {
                let formData = new FormData(form);
                if (Object.keys(this["data"]).length) {
                    for(let i in this["data"]) formData.append(i, this["data"][i]);
                    this["data"] = {};
                }
                formData.append('ID', data["ID"]);

                let xhr = new XMLHttpRequest();
                xhr.open("POST", URL);
                xhr.onloadend = function () {
                    if (xhr.status === 200) {
                        self.setStatus(form, 'waiting', false);
                        self.applyHelper(a, xhr.responseText);
                    }
                };
                xhr.send(formData);
            } else {
                if (Object.keys(this["data"]).length) {
                    for (let k in this["data"]) data[k] = this["data"][k];
                    this["data"] = {};
                }
                u.postJSON(URL, data, function (response) {
                    self.setStatus(form, 'waiting', false);
                    self.applyHelper(a, response);
                });
            }
        } else {
            this.applyChanges();
            this.setActive();
            if (this.callbackApply) this.callbackApply(response);
        }
    }

    clear(a) {
        if (a === undefined) a = this["forms"];
        for (let id in a) {
            let form = a[id];
            u.active(form, false);
            let els = form.querySelectorAll('input, select, textarea');
            els.forEach(function (el) {
                switch (el.tagName.toLowerCase()) {
                    case 'input':
                        if (el.type === 'checkbox') {
                            el.checked = false;
                            let aCheck = el.parentNode.querySelector('[data-id="' + el.name + '"]');
                            if (aCheck) u.active(aCheck, false);
                        } else el.value = '';
                        break;
                    case 'select':
                        el.selectedIndex = 0;
                        break;
                    case 'textarea':
                        el.value = '';
                        break;
                }
            });
        }
    }

    reset() {
        this.resetDefault();
        this.clear();
        this.setInvalids(false);
    }

    setStatus(el, status, v) {
        if (v === undefined) v = true;
        v ? el.classList.add(status) : el.classList.remove(status);
    }

    escapeMsg(str) {
        return str
            .replace(/&/g, '&amp;')
            .replace(/>/g, '&gt;')
            .replace(/</g, '&lt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&apos;');
    }

    activity() {
        const self = this;
        const forms = this["forms"];
        for (let id in forms) {
            let e = forms[id];
            e.onsubmit = function (event) {
                event.preventDefault();
            };

            let els = e.querySelectorAll('input, select, textarea');
            els.forEach(function (el) {
                switch (el.tagName.toLowerCase()) {
                    case 'input':
                        if (el.type === 'checkbox') {
                            el.addEventListener('click', function () {
                                self.fillChanges(el, el.checked ? 'on' : 'off');
                            });
                        } else {
                            el.addEventListener('input', function () {
                                self.fillChanges(el);
                            });
                            el.addEventListener('focus', function () {
                                self.checkInvalid(el);
                            });
                            if (el.type === 'email') {
                                el.addEventListener('blur', function () {
                                    let pattern = /^([а-яa-z0-9_\.-])+@[а-яa-z0-9-]+\.([а-яa-z]{2,}\.)?[а-яa-z]{2,}$/i;
                                    if (!pattern.test(el.value)) {
                                        id = el.closest('form').getAttribute('data-id');
                                        let p = self["invalid"][id];
                                        if (!p) p = self["invalid"][id] = {};
                                        p[el.name] = true;
                                    }
                                });
                            }
                        }
                        break;
                    case 'textarea':
                        el.addEventListener('input', function () {
                            self.fillChanges(el);
                        });
                        el.addEventListener('focus', function () {
                            self.checkInvalid(el);
                        });
                        break;
                    case 'select':
                        el.addEventListener('change', function () {
                            self.fillChanges(el, [el.selectedIndex, el.value]);
                        });
                        break;
                }
                el.addEventListener('click', function () {
                    self.checkInvalid(el);
                });
            });

            let passGenButton = e.querySelectorAll('.password-generate-button');
            if (passGenButton.length) {
                passGenButton.forEach(function (el) {
                    el.addEventListener('click', function () {
                        let input = el.closest('form').querySelector('[name="' + el.getAttribute('data-id') + '"]');
                        input.value = u.passwordGenerate(self.passGenLimits);
                        self.fillChanges(input);
                        self.checkInvalid(input);
                    });
                });
            }

            let submitButton = e.querySelector('button[type=submit]');
            if (submitButton) {
                submitButton.addEventListener('click', function () {
                    let form = submitButton.closest('form');
                    if (form.classList.contains('active')) self.apply();
                });
            }
        }

        if (this["nav"]) {
            this["resetButton"].addEventListener('click', function () {
                self.resetChanges();
            });
            this["submitButton"].addEventListener('click', function () {
                self.apply();
            });
        }
    }
}

class CPagination {
    constructor() {
        const params = {
            limit: 50,
            total: 0,
            page: 1,
            width: {
                obj: 0,
                el: 40,
                list: 0
            },
            list: {},
            listCont: {},
            callbackClick: false,
            tpl: {
                reg: '<li@style@><a>@page@</a></li>',
                page: '<li class="active"><span>@page@</span></li>',
                reverse: '<li><a class="reverse">&larr;</a></li>',
                forward: '<li><a class="forward">&rarr;</a></li>'
            },

            inited: false
        };
        Object.assign(this, params);
    }

    init(params) {
        Object.assign(this, params);
        const diff = this.max = Math.floor(this.total / this.limit);
        if (diff !== this.total / this.limit) this.max = diff + 1;

        if (this.max < 2) return false;

        this.create();

        this.width.list = this.max * this.width.el;
        this.width.mini = this.width.el * 7;
        this.width.midi = this.width.el * 11;
        this.width.max = this.width.el * 15;

        this.activity();
        //this.resize();

        this.inited = true;
    }

    create() {
        let html = '',
            page = Number(this.page),
            max = Number(this.max),
            n, style;
        if (page > 1) {
            html += this.selector(page - 1, 'reverse', false);
            style = page > 2 ? 'right' : false;
            html += this.selector(1, '', style);
        }
        n = page - 4;
        if (n > 1) {
            style = n > 2 ? 'left' : false;
            html += this.selector(n, 'max', style);
        }
        n = page - 3;
        if (n > 1)html += this.selector(n, 'max', false);
        n = page - 2;
        if (n > 1) {
            style = n > 2 ? 'left' : false;
            html += this.selector(n, 'midi', style);
        }
        n = page - 1;
        if (n > 1)html += this.selector(n, 'midi', false);
        html += this.selector(page, '', false);
        n = page + 1;
        if (n < max)html += this.selector(n, 'midi', false);
        n = page + 2;
        if (n < max) {
            style = n < (max - 1) ? 'right' : false;
            html += this.selector(n, 'midi', style);
        }
        n = page + 3;
        if (n < max)html += this.selector(n, 'max', false);
        n = page + 4;
        if (n < max) {
            style = page < (max - 1) ? 'right' : false;
            html += this.selector(n, 'max', style);
        }
        if (page < max) {
            style = page < (max - 1) ? 'left' : false;
            html += this.selector(max, '', style);
            html += this.selector(page + 1, 'forward', false);
        }
        this.el.innerHTML = '<ul>' + html + '</ul>';
        this.listCont = this.el.querySelector('ul');
        this.list = this.listCont.querySelectorAll('li');
    }

    selector(n, tpl, cl) {
        let self = this,
            t = '',
            c = '';
        switch (tpl) {
            default:
                t = self.tpl.reg;
                break;
            case 'reverse':
                t = self.tpl.reverse;
                break;
            case 'forward':
                t = self.tpl.forward;
                break;
            case 'midi':
                t = self.tpl.reg;
                c = 'midi';
                break;
            case 'max':
                t = self.tpl.reg;
                c = 'max';
                break;
        }

        if (cl) {
            switch (cl) {
                case 'right':
                    c += ' right';
                    break;
                case 'left':
                    c += ' left';
                    break;
            }
        }
        if (this.page === n) t = this.tpl.page;
        c = c ? ' class="' + c + '"' : '';
        let r = t.split('@style@').join(c);
        return r.split('@page@').join(n);
    }

    /*resize() {
        let self = this,
            c = self.listCont,
            width = self.width,
            cWidth;
        window.resize = function () {
            self.obj.hide();
            self.width.obj = cWidth = self.obj.parent().width();
            c.removeClass();
            if (width.list > cWidth) {
                if (cWidth < width.max)c.addClass('max');
                if (cWidth < width.midi)c.addClass('midi');
                if (cWidth < width.mini)c.addClass('mini');
            }
            self.obj.show();
        };
        window.resize();
    }*/

    activity() {
        const self = this;
        this.list.forEach(function (el) {
            el.onclick = function (e) {
                let t = e.target;
                if (t.tagName.toLowerCase() !== 'a') return;
                let page = t.innerHTML;
                if (self.callbackClick) {
                    e.preventDefault();
                    if (t.classList.contains('reverse')) page = self.page - 1;
                    if (t.classList.contains('forward')) page = self.page + 1;
                    self.callbackClick(page);
                }
            };
        });
    }
}

class CGallery {
    constructor() {
        const params = {
            "el": '',
            "list": [],
            "loader": new CPreLoad(),
            "srcList": [],
            "labels": ['BEFORE', 'AFTER'],
            "type": ''
        };
        Object.assign(this, params);
    }

    init(params) {
        if(params) Object.assign(this, params);

        const list = this["list"];
        this["loader"].insertImage = function (type, img, src) {
            list.push({
                "img": img,
                "w": img.width,
                "h": img.height,
                "src": src
            });
        }

        this.initList();
    }

    initLoader() {
        this["el"].innerHTML = this["loader"].spinner(true, true);
        this["loader"]["loader"] = this["el"].querySelector('.loader');
    }

    initList() {
        const dummy = this["dummy"];
        this["type"] = dummy.getAttribute('data-type');
        this["url"] += (dummy.getAttribute('data-url') || '') + '/';
        const url = this["url"];

        let labels = dummy.getAttribute('data-labels');
        if (labels) this["labels"] = labels.split(',')

        let srcList = [];
        let str = u.clearString(dummy.innerHTML).replace(/ /g, '');
        str.split(',').forEach(function (el) {
            srcList.push(url + el);
        });
        this["srcList"] = srcList;

        this.initEl(dummy);
    }

    initEl(el) {
        el.insertAdjacentHTML('afterend', '<div class="gallery g-t-' + this["type"] + '"></div>');
        this["el"] = el.nextSibling;
        el.remove();

        this["el"].onselectstart = function() {
            return false;
        };

        this.initLoader();
        this.loadImages();
    }

    loadImages(a) {
        if (!a) a = this["srcList"];
        const src = a.shift();
        if (src) {
            const self = this;
            this["loader"].loadImage(self["attr-img"], src, function () {
                a ? self.loadImages(a) : self.create();
            });
        } else this.create();
    }

    renderImage(el) {
        return '<img src="' + el["src"] + '" alt="' + (el["alt"] || '') + '" />';
    }

    setSize() {
        const cont = this["cont"];
        const ms = this["main-sizes"];
        const s = this["sizes"];

        cont.removeAttribute("style");

        let q = ms["w"] / cont.offsetWidth;
        if (q > 1) {
            s["w"] = cont.offsetWidth;
            s["h"] = ms["h"] / q;
        } else {
            s["w"] = ms["w"];
            s["h"] = ms["h"];
        }

        cont.style["width"] = s["w"] + 'px';
        cont.style["height"] = s["h"] + 'px';

        s["l"] = cont.getBoundingClientRect().left;
    }

    setSlider() {
        const s = this["sizes"];

        this["thumb"].style["left"] = (s["w"] /2 - this["thumb"].offsetWidth /2) + 'px';
        this["thumb"].style["top"] = (s["h"] /2 - this["thumb"].offsetHeight /2) + 'px';

        this["wrap"].style["width"] = s["w"] + 'px';
        this["wrap"].style["height"] = s["h"] + 'px';

        this["cover"].style["width"] = s["w"] /2 + 'px';
    }

    create() {
        const list = this["list"];
        let main = list[0];
        this["sizes"] = {"w": main["w"], "h": main["h"]};
        this["main-sizes"] = {"w": main["w"], "h": main["h"]};

        let cont = document.createElement('div');
        cont.className = 'g-cont';
        this["el"].append(cont);
        this["cont"] = cont;
        u.wait(cont);

        this.setSize();

        if (this["type"] === 'b-a' || this["type"] === 'b-a-slider' || this["type"] === 'b-a-combo') {
            let afterLabel, beforeLabel, counter, slider, sliderThumb, toggle, toggleWidth, func = false;

            function action(type) {
                switch (type) {
                    case 'slider':
                        if (counter > 50) {
                            u.active(afterLabel);
                            u.active(beforeLabel, false);
                        } else {
                            u.active(beforeLabel);
                            u.active(afterLabel, false);
                        }
                        break;
                    case 'left':
                        sliderThumb.innerHTML = '0';
                        sliderThumb.style["left"] = '0%';
                        break;
                    case 'right':
                        sliderThumb.innerHTML = '100';
                        sliderThumb.style["left"] = '100%';
                        break;
                }
            }

            let before = document.createElement('div');
            before.className = 'g-before';
            before.innerHTML = this.renderImage(main);
            u.active(before);
            cont.append(before);

            let after = document.createElement('div');
            after.className = 'g-after';
            after.innerHTML = this.renderImage(list[1]);
            cont.append(after);

            let toggleCont = document.createElement('div');
            toggleCont.className = 'g-toggle';
            cont.append(toggleCont);
            toggleWidth = toggleCont.offsetWidth;

            if (this["type"] === 'b-a' || this["type"] === 'b-a-combo') {
                if (this["type"] === 'b-a-combo') func = action;

                toggle = document.createElement('div');
                toggle.className = 'toggle';
                toggleCont.append(toggle);

                beforeLabel = document.createElement('span');
                beforeLabel.className = 'link-before active';
                beforeLabel.innerHTML = this["labels"][0];
                toggle.append(beforeLabel);

                let icon = document.createElement('a');
                icon.className = 'i-icon';
                icon.onclick = function () {
                    if (u.active(beforeLabel, 'is')) {
                        after.style["opacity"] = '';
                        before.style["opacity"] = '';
                        u.active(afterLabel);
                        u.active(beforeLabel, false);
                        u.active(before, false);
                        if (func) func('right');
                    } else {
                        after.style["opacity"] = '';
                        before.style["opacity"] = '';
                        u.active(beforeLabel);
                        u.active(afterLabel, false);
                        u.active(before);
                        if (func) func('left');
                    }
                }
                toggle.append(icon);

                afterLabel = document.createElement('span');
                afterLabel.className = 'link-after';
                afterLabel.innerHTML = this["labels"][1];
                toggle.append(afterLabel);
            }

            if (this["type"] === 'b-a-slider' || this["type"] === 'b-a-combo') {
                if (this["type"] === 'b-a-combo') func = action;

                slider = document.createElement('div');
                slider.className = 'g-slider';
                slider.style["width"] = toggleCont.offsetWidth + 'px';
                toggleCont.append(slider);

                sliderThumb = document.createElement('span');
                sliderThumb.className = 'g-slider-thumb';
                slider.append(sliderThumb);

                let sliderThumbHalf = sliderThumb.offsetWidth /2;

                function sliderThumbDown() {
                    document.addEventListener('mousemove', sliderThumbMoveBound, false);
                    document.addEventListener('mouseup', sliderThumbUp, false);
                }

                let sliderThumbMoveBound = sliderThumbMove.bind(self, this);
                function sliderThumbMove(self, e) {
                    let left = (e.changedTouches ? e.changedTouches[0].clientX : e.clientX) - toggleCont.offsetLeft - self["sizes"]["l"];
                    if (left < -sliderThumbHalf) left = -sliderThumbHalf;
                    let rightEdge = toggleWidth - sliderThumbHalf;
                    if (left > rightEdge) left = rightEdge;

                    counter = Math.ceil((left + sliderThumbHalf) / toggleWidth * 100);
                    sliderThumb.innerHTML = counter;
                    sliderThumb.style["left"] = (left + sliderThumbHalf) + 'px';

                    if (func) func('slider');

                    let less = counter / 100;
                    before.style["opacity"] = 1 - less;
                }

                function sliderThumbUp() {
                    document.removeEventListener('mousemove', sliderThumbMoveBound, false);
                    document.removeEventListener('mouseup', sliderThumbUp, false);

                    u.active(cont, false);
                }

                sliderThumb.ondragstart = function() {
                    return false;
                };

                sliderThumb.onmousedown = sliderThumbDown;

                sliderThumb.addEventListener('touchstart', sliderThumbDown, false);
                sliderThumb.addEventListener('touchend', sliderThumbUp, false);
                sliderThumb.addEventListener('touchmove', sliderThumbMoveBound, false);

                action('left');
            }
        }
        else if (this["type"] === 'slider') {
            let underCover = document.createElement('div');
            underCover.className = 'g-under-cover';
            underCover.innerHTML = this.renderImage(list[1]);
            cont.append(underCover);

            let wrap = document.createElement('div');
            wrap.className = 'wrap';
            wrap.innerHTML = this.renderImage(main);
            this["wrap"] = wrap;

            let cover = document.createElement('div');
            cover.className = 'g-cover';
            this["cover"] = cover;

            cover.append(wrap);
            cont.append(cover);

            let thumb = document.createElement('span');
            thumb.className = 'g-slider-thumb';
            thumb.innerHTML = '<i class="i-icon"></i>'
            cont.append(thumb);
            this["thumb"] = thumb;
            let thumbWidth = thumb.offsetWidth, thumbHalf = thumbWidth /2;
            this.setSlider();

            function thumbDown() {
                document.addEventListener('mousemove', thumbMoveBound, false);
                document.addEventListener('mouseup', thumbUp, false);
            }

            let thumbMoveBound = thumbMove.bind(self, this);
            function thumbMove(self, e) {
                u.active(cont);
                let left = (e.changedTouches ? e.changedTouches[0].clientX : e.clientX) - self["sizes"]["l"] - thumbHalf;
                if (left < -thumbHalf) left = -thumbHalf;
                let rightEdge = self["sizes"]["w"] - thumbHalf;
                if (left > rightEdge) left = rightEdge;
                thumb.style["left"] = left + 'px';
                cover.style["width"] = (left + thumbHalf) + 'px';
            }

            function thumbUp() {
                document.removeEventListener('mousemove', thumbMoveBound);
                document.removeEventListener('mouseup', thumbUp);

                u.active(cont, false);
            }

            thumb.ondragstart = function() {
                return false;
            };

            thumb.onmousedown = thumbDown;

            thumb.addEventListener('touchstart', thumbDown, false);
            thumb.addEventListener('touchend', thumbUp, false);
            thumb.addEventListener('touchmove', thumbMoveBound, false);
        }
        else if (this["type"] === 'preview') return;

        this["loader"].spinner(false);
        u.wait(cont, false);

        if (this.callbackInit) this.callbackInit();
    }
}

function setDataLinks() {
    const a = document.querySelectorAll('[data-page], [data-scroll]');
    if (a.length) {
        a.forEach(function (el) {
            el.onclick = function (e) {
                e.preventDefault();
                let selector = el.getAttribute('data-scroll');
                let cSelector = el.hasAttribute('data-click');
                if (cSelector) {
                    cSelector = el.getAttribute('data-click');
                    if (!cSelector) cSelector = selector;
                }
                let pSelector = el.getAttribute('data-page');
                if (pSelector) {
                    if (selector) localStorage.setItem('scrollID', selector);
                    if (cSelector) localStorage.setItem('clickID', cSelector);
                    location.href = pSelector;
                } else {
                    if (selector) u.scrollTo(selector, gap);
                    if (cSelector) document.querySelector(cSelector).click();
                    if (typeof dataLinksCallback == 'function') dataLinksCallback(selector);
                }
            };
        });
    }

    let selector = localStorage.getItem('scrollID');
    if (selector) {
        u.scrollTo(selector, gap);
        localStorage.removeItem('scrollID');
        if (typeof dataLinksCallback == 'function') dataLinksCallback(selector);
    }
    let cSelector = localStorage.getItem('clickID');
    if (cSelector) {
        document.querySelector(cSelector).click();
        localStorage.removeItem('clickID');
    }
}

function setListeners() {
    if (WLA) {
        for (let n = 0; n < WLA.length; n++) {
            let e = WLA[n];
            for (let param in e) window.addEventListener(param, e[param]);
        }
    }
}

function setPLA(a, callback) {
    let el = a.shift();
    if (el) {
        el();
        setPLA(a, callback);
    } else if (callback) callback();
}

function setToggle () {
    const toggles = document.querySelectorAll('.a-toggle');
    if (toggles.length) {
        toggles.forEach(function (e){
            let pID = e.getAttribute('data-parent'),
                p = pID ? document.getElementById(pID) : e.parentNode,
                id = e.getAttribute('data-id'),
                active = false;
            let target = document.getElementById(id);
            if (!target) target = e.parentNode;
            if(id) {
                let def = e.getAttribute('data-default');
                if (def) active = def === 'true';
                let lsa = localStorage.getItem(id);
                if (lsa) active = lsa === 'true';
                u.active(target, active);
                u.active(p, active);
            }
            e.addEventListener('click', function () {
                pID = this.getAttribute('data-parent');
                p = pID ? document.getElementById(pID) : this.parentNode;
                id = this.getAttribute('data-id');
                target = document.getElementById(id);
                if (!target) target = e.parentNode;
                active = u.active(p, 'toggle');
                if(id) localStorage.setItem(id, active);
                u.active(target, active);
            });
        });
    }
}

function setVP() {
    const vp = u.viewport();
    const cl = body.classList;
    cl.remove('vp-xs');
    cl.remove('vp-sm');
    cl.remove('vp-md');
    cl.remove('vp-lg');
    cl.remove('vp-mx');
    cl.add('vp-' + vp["vp"]);
}

function slideHide(el, h, func, step) {
    let s = step ?? slideStep;
    el.style["height"] = h + 'px';
    let start = h;
    let ls = setInterval(function () {
        start -= s;
        if (start < 0) start = 0;
        el.style["height"] = start + 'px';
        if (!start) {
            clearInterval(ls);
            el.style["display"] = 'none';
            if (func) func();
        }
    }, slideTimeout);
}

function slideShow(el, h, func, step) {
    let s = step ?? slideStep;
    el.style["display"] = 'block';
    let start = 0;
    let ls = setInterval(function () {
        start += s;
        el.style["height"] = start + 'px';
        if (start > h) start = h;
        if (start === h) {
            clearInterval(ls);
            el.removeAttribute('style');
            if (func) func();
        }
    }, slideTimeout);
}

function copyToClipboard(str) {
    const ta = document.createElement('textarea');
    ta.value = str;
    ta.style.left = '-9999px';
    ta.style.position = 'absolute';
    ta.setAttribute('readonly', '');
    document.body.appendChild(ta);
    ta.select();
    document.execCommand('copy');
    document.body.removeChild(ta);
}

function share(id, data) {
    const a = JSON.parse(data);
    let url = '', popup = true;
    switch(id) {
        case 'pinterest':
            url += 'https://pinterest.com/pin/create/link/?';
            url += 'url='          + encodeURIComponent(a[0]);
            url += '&description=' + encodeURIComponent(a[1]);
            break;
        case 'linkedin':
            url += 'https://www.linkedin.com/shareArticle?mini=true&';
            url += 'url='          + encodeURIComponent(a[0]);
            url += '&title='       + encodeURIComponent(a[1]);
            url += '&summary='     + encodeURIComponent(a[3]);
            url += '&source='       + encodeURIComponent(a[2]);
            break;
        case 'vkontakte':
            url += 'https://vkontakte.ru/share.php?';
            url += 'url='          + encodeURIComponent(a[0]);
            url += '&title='       + encodeURIComponent(a[1]);
            url += '&description=' + encodeURIComponent(a[3]);
            url += '&image='       + encodeURIComponent(a[2]);
            url += '&noparse=true';
            break;
        case 'odnoklassniki':
            url += 'https://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(a[1]);
            url += '&st._surl='    + encodeURIComponent(a[0]);
            break;
        case 'facebook':
            url += 'https://www.facebook.com/sharer.php?s=100';
            url += '&p[title]='     + encodeURIComponent(a[1]);
            url += '&p[summary]='   + encodeURIComponent(a[3]);
            url += '&p[url]='       + encodeURIComponent(a[0]);
            url += '&p[images][0]=' + encodeURIComponent(a[2]);
            break;
        case 'twitter':
            url += 'https://twitter.com/share?';
            url += 'text='          + encodeURIComponent(a[1]);
            url += '&url='          + encodeURIComponent(a[0]);
            url += '&counturl='     + encodeURIComponent(a[0]);
            break;
        case 'mailru':
            url += 'https://connect.mail.ru/share?';
            url += 'url='          + encodeURIComponent(a[0]);
            url += '&title='       + encodeURIComponent(a[1]);
            url += '&description=' + encodeURIComponent(a[3]);
            url += '&imageurl='    + encodeURIComponent(a[2]);
            break;
        case 'permalink':
            copyToClipboard(a[0]);
            popup = false;
            break;
    }

    if (popup) window.open(url, '', 'toolbar=0, status=0, width=626, height=436');
}