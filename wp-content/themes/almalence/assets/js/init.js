const mel = wrapper;
PL = new CPreLoadMedia();
PL.callback = function (a) {
    setPLA(PLA, setListeners);
};

const fontsDir = URI_TPL + 'fonts/';
const siteFonts = [
    {"title": 'Europe-Regular', "ext": 'ttf'},
    {"title": 'Quicksand-Regular', "ext": 'ttf'},
    {"title": 'Quicksand-Medium', "ext": 'ttf'},
    {"title": 'Quicksand-Bold', "ext": 'ttf'},
    {"title": 'OpenSans-Light', "ext": 'ttf'},
    {"title": 'OpenSans-Regular', "ext": 'ttf'},
    {"title": 'OpenSans-Bold', "ext": 'ttf'}
];
if ('fonts' in document) {
    let fonts = [{"title": 'FontAwesome', "ext": 'ttf'}];
    let a = [];
    fonts.forEach(function (el) {
        let title = el["title"];
        a.push(new FontFace(title, 'url("' + fontsDir + title + '.' + el["ext"] + '")').load());
    });
    Promise.all(a).then(function (loadedFonts) {
        loadedFonts.forEach(function (font) {
            document.fonts.add(font);
        });
        PL.init(mel, true);

        fonts = [{"title": 'FontAwesome-Alt', "ext": 'ttf'}, {"title": 'FontAwesome-Brands', "ext": 'ttf'}];
        let a = [];
        fonts.forEach(function (el) {
            let title = el["title"];
            a.push(new FontFace(title, 'url("' + fontsDir + title + '.' + el["ext"] + '")').load());
        });
        Promise.all(a).then(function (loadedFonts) {
            loadedFonts.forEach(function (font) {
                document.fonts.add(font);
            });

            // site-fonts
            if (siteFonts.length) {
                let a = [];
                siteFonts.forEach(function (el) {
                    let title = el["title"];
                    a.push(new FontFace(title, 'url("' + fontsDir + title + '.' + el["ext"] + '")').load());
                });
                Promise.all(a).then(function (loadedFonts) {
                    loadedFonts.forEach(function (font) {
                        document.fonts.add(font);
                    });
                    PL.init(mel);
                });
            } else PL.init(mel);
        });
    });
} else {
    PL.init(mel, true);
}