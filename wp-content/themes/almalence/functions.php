<?php
const THEME_VERSION = '1-1';
const DUMMY_ID = 1;
const POSTS_PER_PAGE = 5;
const TOPIC_TITLE = 'topic';
const TOPIC_PFX = TOPIC_TITLE . '-';
const MAIN_PAGE_TITLE = 'Almalence Inc &mdash; Global leader in mobile imaging and computational photography';
const LINK_VIMEO = 'https://vimeo.com/654000718';
const LINK_LINKEDIN = 'https://linkedin.com/';

function setHeadTitle($ID = false)
{
    global $wp;
    global $post;

    if ($ID) $title = get_the_title($ID);
    else {
        $args = URI2Array($wp->matched_query);
        $title = 'SOME_TITLE';
        if ($args) {
            if (isset($args["tag"])) $title = single_tag_title('', false);
            elseif (isset($args["category_name"])) $title = single_cat_title('', false);
            elseif (isset($args["name"])) $title = $post->post_title;
        }
    }
    return $title;
}

function setPostLink($tags, $ID = false)
{
    $main = false;
    foreach ($tags as $tag) {
        $name = $tag->name;
        $a = explode('-main', $name);
        if (count($a) > 1) {
            $b = explode(TOPIC_PFX, $a[0]);
            if (count($b) > 1) $main = $b[1];
        }
    }
    return $main ? '/' . TOPIC_TITLE . '/' . $main . '/' : esc_url(get_permalink($ID));
}

function setPostCategories()
{
    $a = [];
    if (has_category()) {
        $cats = get_the_category();
        foreach ($cats as $cat) $a[] = '<li><a href="/category/' . $cat->slug . '">' . $cat->name . '</a></li>';
        return implode('', $a);
    }
}

function viewPostCategories()
{
    $list = setPostCategories();
    echo $list ? '<div class="post-cats"><i class="i-list"></i> <ul class="list-">' . $list . '</ul></div>' : '';
}

function viewPostCommentsCount()
{
    return '<a href="#comments" class="a-comments">' . get_comments_number() . '</a>';
}

function viewLogo()
{
    return '<a href="/" class="logo"><img src="' . getURI() . 'images/logo.svg" class="logo-img" alt=""/><span class="logo-title"><span class="logo-title-name">Almalence</span> <span class="logo-title-motto">Innovative Imaging Technologies</span></span></a>';
}

// post' thumbnail
function getPostExcerptThumbnail($ID)
{
    $tID = has_post_thumbnail() ? $ID : DUMMY_ID;
    $url = wp_get_attachment_image_url(get_post_thumbnail_id($tID), 'large');
    return 'background-image: url(' . $url . ')';
}

function viewPostArticleThumbnail($ID)
{
    $post = false;
    if (is_object($ID)) {
        $post = $ID;
        $ID = $post->ID;
    }
    $tID = has_post_thumbnail($post) ? $ID : DUMMY_ID;
    $url = wp_get_attachment_image_url(get_post_thumbnail_id($tID), 'medium');
    return 'background-image: url(\'' . $url . '\')';
}

function viewPostPinThumbnail()
{
    echo '<figure>' . (has_post_thumbnail() ?  get_the_post_thumbnail(null, 'post-thumbnail', array('loading' => false)) : get_the_post_thumbnail(DUMMY_ID)) . '</figure>';
}

// post' tags
function checkTag2Topic($title)
{
    return get_tags(["name__like" => TOPIC_PFX . $title . '-main']) ? '/' . TOPIC_TITLE . '/' . $title: false;
}

function setTags($tags)
{
    $a = [];
    foreach ($tags as $tag) {
        $slug = $tag->slug;
        if (strpos($slug, TOPIC_PFX) === false) $a[] = '<li><a href="/tag/' . $slug . '">' . $tag->name . '</a></li>';
    }
    return implode('', $a);
}

function viewPostTags($tags)
{
    echo $tags ? '<div class="post-tags"><i class="i-tags"></i><ul class="list-">' . setTags($tags) . '</ul></div>' : '';
}

// post' views
function checkPostContent($ID)
{
    $c = get_the_content($ID);
    return $c && $c !== '';
}

function getPostViews($ID)
{
    $key = 'post_views_count';
    $count = get_post_meta($ID, $key, true);
    if ($count == '') {
        delete_post_meta($ID, $key);
        add_post_meta($ID, $key, 1);
    }
    return $count;
}

function setPostViews($ID)
{
    $key = 'post_views_count';
    $count = get_post_meta($ID, $key, true);
    if ($count == '') {
        delete_post_meta($ID, $key);
        add_post_meta($ID, $key, 1);
    } else {
        $count++;
        update_post_meta($ID, $key, $count);
    }
    return true;
}

function viewPostViews($ID)
{
    if (setPostViews($ID)) return '<span class="a-views">' . getPostViews($ID) . '</span>';
}

// links
function viewContacts()
{
    return '<nav class="social"><a href="' . LINK_LINKEDIN . '" title="Go to our LinkedIn"><i class="ib-linkedin"></i></a>
    <a href="' . LINK_VIMEO . '" title="Go to our Vimeo\' chanel"><i class="ib-vimeo"></i></a>
    <a data-id="contact" title="Contact us"><i class="i-envelope"></i></a>
    </nav>';
}

function viewSocials()
{
    return '<nav class="social"><a data-id="pinterest" data-data=\'' . json_encode(["URL","TITLE"]) . '\' title="Share on Pinterest"><i class="ib-pinterest"></i></a>
    <a data-id="linkedin" data-data=\'' . json_encode(["URL","TITLE","Almalence Inc.","DESC"]) . '\' title="Share on LinkedIn"><i class="ib-linkedin"></i></a>
    <a data-id="facebook" data-data=\'' . json_encode(["URL","TITLE","IMG_PATH","DESC"]) . '\' title="Share on Facebook"><i class="ib-facebook"></i></a>
    <a data-id="twitter" data-data=\'' . json_encode(["URL","TITLE"]) . '\' title="Share on Twitter"><i class="ib-twitter"></i></a>
    </nav>';
}

function viewLinkMorePosts($a)
{
    return '<span class="watch-mark" data-func="loadPosts" data-attr=\'' . json_encode($a) . '\'></span>';
}

function viewPostShare($ID)
{
    echo '<div class="post-share"><a data-id="pinterest" data-data=\'' . json_encode(["URL","TITLE"]) . '\' title="Share on Pinterest"><i class="ib-pinterest"></i></a>
    <a data-id="linkedin" data-data=\'' . json_encode(["URL","TITLE","Almalence Inc.","DESC"]) . '\' title="Share on LinkedIn"><i class="ib-linkedin"></i></a>
    <a data-id="facebook" data-data=\'' . json_encode(["URL","TITLE","IMG_PATH","DESC"]) . '\' title="Share on Facebook"><i class="ib-facebook"></i></a>
    <a data-id="twitter" data-data=\'' . json_encode(["URL","TITLE"]) . '\' title="Share on Twitter"><i class="ib-twitter"></i></a>
    <a data-id="permalink" data-data=\'' . json_encode([get_permalink($ID)]) . '\' title="Click to copy permalink"><i class="i-link"></i></a>
    </div>';
}

function viewLinkViewMore($ID)
{
    return checkPostContent($ID) ? '<a class="a-view-more" data-id="' . $ID . '">View more</a><i class="i-waiting"></i>' : '';
}

function getPostNavLink($ID)
{
    $tags = wp_get_post_tags($ID);
    return setPostLink($tags, $ID);
}

function viewPostNavHelper($e, $link)
{
    return '<article>
        <header>
            <a href="' . $link . '">
                <figure class="thumbnail" style="' . viewPostArticleThumbnail($e) . '"></figure>
                <h2>' . $e->post_title . '</h2>
            </a>
        </header>
	</article>';
}

function viewPostNav($topic, $prev, $next)
{
    $linkPrev = getPostNavLink($prev->ID);
    $header = viewPostNavHelper($prev, $linkPrev);
    $linkNext = getPostNavLink($next->ID);
    $header .= viewPostNavHelper($next, $linkNext);

    if ($topic) {
        $linkTopic = getPostNavLink($topic->ID);
        $footer = viewPostNavHelper($topic, $linkTopic);
    }

    $body = '<a href="' . $linkPrev . '" title="Go to previous post" class="a-prev">prev</a>';
    if ($topic) $body .= '<a href="' . $linkTopic . '" title="Go to topic" class="a-topic">topic</a>';
    $body .= '<a href="' . $linkNext . '" title="Go to next post" class="a-next">next</a>';

    $nav = '<header>' . $header . '</header>';
    $nav .= '<nav>' . $body . '</nav>';
    if ($topic) $nav .= '<footer>' . $footer . '</footer>';
    return '<nav class="nav-post">' . $nav . '</nav>';
}

// utils
function arrayFill($a)
{
    $b = [];
    foreach ($a as $v) $b[] = $v;
    return $b;
}

function getArgs($e = false)
{
    global $wp;
    $args = URI2Array($wp->matched_query);
    return $e ? $args[$e] : $args;
}

function getInc($name)
{
    return get_template_directory() . '/includes/' . $name . '.php';
}

function getRandom($a, $n) {
    $b = array_rand($a, $n);
    $c = [];
    foreach ($b as $i) $c[] = $a[$i];
    return $c;
}

function getURI($dir = 'assets')
{
    $tail = $dir == 'assets' ? THEME_VERSION . '/' : '';
    return '/wp-content/themes/almalence/' . $dir . '/' . $tail;
}

function setCategoryList($a, $active = '')
{
    $str = '';
    foreach($a as $v) {
        $selected = $v["slug"] == $active ? ' class="active"' : '';
        $str .= '<li' . $selected . '><a href="/category/' . $v["slug"] . '/">' . $v["name"] . '</a></li>';
        if (isset($v["children"])) {
            $str .= '<ul>';
            foreach($v["children"] as $e) {
                $selected = $e["slug"] == $active ? ' class="active"' : '';
                $str .= '<li' . $selected . '><a href="/category/' . $v["slug"] . '/' . $e["slug"] . '/">' . $e["name"] . '</a></li>';
            }
            $str .= '</ul>';
        }
    }
    return '<ul class="list-cats">' . $str . '</ul>';
}

function setCategorySelect($a, $active)
{
    $str = '';
    foreach($a as $v) {
        $selected = $v["slug"] == $active ? ' selected' : '';
        $str .= '<option value="' . $v["slug"] . '"' . $selected . '>' . $v["name"] . '</option>';
        if (isset($v["children"])) {
            foreach($v["children"] as $el) {
                $selected = $el["slug"] == $active ? ' selected' : '';
                $str .= '<option value="' . $v["slug"] . '/' . $el["slug"] . '"' . $selected . '>&mdash;&#160;' . $el["name"] . '</option>';
            }
        }
    }
    return '<select>' . $str . '</select>';
}

function setTagsList($active)
{
    $a = [];
    $tags = get_tags();
    foreach ($tags as $tag) {
        $slug = $tag->slug;
        if (strpos($slug, TOPIC_PFX) === false) {
            $el = $slug == $active ? '<strong>' . $tag->name . '</strong>' : '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
            $a[] = '<li>' . $el . '</li>';
        }
    }
    return implode('', $a);
}

function renderTagsList($active = '')
{
    return '<div class="container"><span class="tags-label"><i class="i-tags"></i> All tags</span><ul class="list-tags">' . setTagsList($active) . '</ul></div>';
}

function URI2Array($URI)
{
    parse_str($URI, $a);
    return $a;
}

// navs
add_filter('nav_menu_css_class', '__return_empty_array');
add_filter('nav_menu_item_id', '__return_empty_string');

// actions
function redirectRules()
{
    add_rewrite_rule(TOPIC_TITLE . '/([^/]+)/?', 'index.php?t=$matches[1]', 'top');
    add_rewrite_rule('categories', 'index.php?categories', 'top');
    add_rewrite_rule('rules', 'index.php?page=rules', 'top');
    add_rewrite_rule('sitemap', 'index.php?sitemap', 'top');
    add_rewrite_rule('tags', 'index.php?tags', 'top');
    flush_rewrite_rules();
}
add_action('init', 'redirectRules');

function start_session()
{
    if(!session_id()) session_start();
}
add_action('init', 'start_session', 1);

// useless?
function viewPostReadMore()
{
    return esc_html__('read more', 'almalence');
}

function almalence_continue_reading_link_excerpt() {
    /*if ( ! is_admin() ) {
        return '&hellip; <a class="more-link" href="' . esc_url( get_permalink() ) . '">' . viewPostReadMore() . '</a>';
    }*/
}
add_filter( 'excerpt_more', 'almalence_continue_reading_link_excerpt' );

// debug
function debug()
{
    global $wp;
    foreach ($wp as $k => $v) {
        echo '<h3>' . $k . '</h3>';
        var_export($v);
        echo '<br /><br />';
    }
}
/// debug


// This theme requires WordPress 5.3 or later.
if (version_compare($GLOBALS['wp_version'], '5.3', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

if (!function_exists('almalence_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @return void
     * @since Almalence 1.0
     *
     */
    function almalence_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Almalence, use a find and replace
         * to change 'almalence' to the name of your theme in all the template files.
         */
        load_theme_textdomain('almalence', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * This theme does not use a hard-coded <title> tag in the document head,
         * WordPress will provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Add post-formats support.
         */
        add_theme_support(
            'post-formats',
            array(
                'link',
                'aside',
                'gallery',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            )
        );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1568, 9999);

        register_nav_menus(
            array(
                'primary' => esc_html__('Primary menu', 'almalence'),
                'footer' => __('Secondary menu', 'almalence'),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
                'navigation-widgets',
            )
        );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        $logo_width = 300;
        $logo_height = 100;

        add_theme_support(
            'custom-logo',
            array(
                'height' => $logo_height,
                'width' => $logo_width,
                'flex-width' => true,
                'flex-height' => true,
                'unlink-homepage-logo' => true,
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');
        $background_color = get_theme_mod('background_color', 'D1E4DD');
        if (127 > Almalence_Custom_Colors::get_relative_luminance_from_hex($background_color)) {
            add_theme_support('dark-editor-style');
        }

        $editor_stylesheet_path = './assets/css/style-editor.css';

        // Note, the is_IE global variable is defined by WordPress and is used
        // to detect if the current browser is internet explorer.
        global $is_IE;
        if ($is_IE) {
            $editor_stylesheet_path = './assets/css/ie-editor.css';
        }

        // Enqueue editor styles.
        add_editor_style($editor_stylesheet_path);

        // Add custom editor font sizes.
        add_theme_support(
            'editor-font-sizes',
            array(
                array(
                    'name' => esc_html__('Extra small', 'almalence'),
                    'shortName' => esc_html_x('XS', 'Font size', 'almalence'),
                    'size' => 16,
                    'slug' => 'extra-small',
                ),
                array(
                    'name' => esc_html__('Small', 'almalence'),
                    'shortName' => esc_html_x('S', 'Font size', 'almalence'),
                    'size' => 18,
                    'slug' => 'small',
                ),
                array(
                    'name' => esc_html__('Normal', 'almalence'),
                    'shortName' => esc_html_x('M', 'Font size', 'almalence'),
                    'size' => 20,
                    'slug' => 'normal',
                ),
                array(
                    'name' => esc_html__('Large', 'almalence'),
                    'shortName' => esc_html_x('L', 'Font size', 'almalence'),
                    'size' => 24,
                    'slug' => 'large',
                ),
                array(
                    'name' => esc_html__('Extra large', 'almalence'),
                    'shortName' => esc_html_x('XL', 'Font size', 'almalence'),
                    'size' => 40,
                    'slug' => 'extra-large',
                ),
                array(
                    'name' => esc_html__('Huge', 'almalence'),
                    'shortName' => esc_html_x('XXL', 'Font size', 'almalence'),
                    'size' => 96,
                    'slug' => 'huge',
                ),
                array(
                    'name' => esc_html__('Gigantic', 'almalence'),
                    'shortName' => esc_html_x('XXXL', 'Font size', 'almalence'),
                    'size' => 144,
                    'slug' => 'gigantic',
                ),
            )
        );

        // Custom background color.
        add_theme_support(
            'custom-background',
            array(
                'default-color' => 'd1e4dd',
            )
        );

        // Editor color palette.
        $black = '#000000';
        $dark_gray = '#28303D';
        $gray = '#39414D';
        $green = '#D1E4DD';
        $blue = '#D1DFE4';
        $purple = '#D1D1E4';
        $red = '#E4D1D1';
        $orange = '#E4DAD1';
        $yellow = '#EEEADD';
        $white = '#FFFFFF';

        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name' => esc_html__('Black', 'almalence'),
                    'slug' => 'black',
                    'color' => $black,
                ),
                array(
                    'name' => esc_html__('Dark gray', 'almalence'),
                    'slug' => 'dark-gray',
                    'color' => $dark_gray,
                ),
                array(
                    'name' => esc_html__('Gray', 'almalence'),
                    'slug' => 'gray',
                    'color' => $gray,
                ),
                array(
                    'name' => esc_html__('Green', 'almalence'),
                    'slug' => 'green',
                    'color' => $green,
                ),
                array(
                    'name' => esc_html__('Blue', 'almalence'),
                    'slug' => 'blue',
                    'color' => $blue,
                ),
                array(
                    'name' => esc_html__('Purple', 'almalence'),
                    'slug' => 'purple',
                    'color' => $purple,
                ),
                array(
                    'name' => esc_html__('Red', 'almalence'),
                    'slug' => 'red',
                    'color' => $red,
                ),
                array(
                    'name' => esc_html__('Orange', 'almalence'),
                    'slug' => 'orange',
                    'color' => $orange,
                ),
                array(
                    'name' => esc_html__('Yellow', 'almalence'),
                    'slug' => 'yellow',
                    'color' => $yellow,
                ),
                array(
                    'name' => esc_html__('White', 'almalence'),
                    'slug' => 'white',
                    'color' => $white,
                ),
            )
        );

        add_theme_support(
            'editor-gradient-presets',
            array(
                array(
                    'name' => esc_html__('Purple to yellow', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $yellow . ' 100%)',
                    'slug' => 'purple-to-yellow',
                ),
                array(
                    'name' => esc_html__('Yellow to purple', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $purple . ' 100%)',
                    'slug' => 'yellow-to-purple',
                ),
                array(
                    'name' => esc_html__('Green to yellow', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $green . ' 0%, ' . $yellow . ' 100%)',
                    'slug' => 'green-to-yellow',
                ),
                array(
                    'name' => esc_html__('Yellow to green', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $green . ' 100%)',
                    'slug' => 'yellow-to-green',
                ),
                array(
                    'name' => esc_html__('Red to yellow', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $yellow . ' 100%)',
                    'slug' => 'red-to-yellow',
                ),
                array(
                    'name' => esc_html__('Yellow to red', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $red . ' 100%)',
                    'slug' => 'yellow-to-red',
                ),
                array(
                    'name' => esc_html__('Purple to red', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $red . ' 100%)',
                    'slug' => 'purple-to-red',
                ),
                array(
                    'name' => esc_html__('Red to purple', 'almalence'),
                    'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $purple . ' 100%)',
                    'slug' => 'red-to-purple',
                ),
            )
        );

        /*
        * Adds starter content to highlight the theme on fresh sites.
        * This is done conditionally to avoid loading the starter content on every
        * page load, as it is a one-off operation only needed once in the customizer.
        */
        if (is_customize_preview()) {
            require get_template_directory() . '/inc/starter-content.php';
            add_theme_support('starter-content', almalence_get_starter_content());
        }

        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');

        // Add support for custom line height controls.
        add_theme_support('custom-line-height');

        // Add support for experimental link color control.
        add_theme_support('experimental-link-color');

        // Add support for experimental cover block spacing.
        add_theme_support('custom-spacing');

        // Add support for custom units.
        // This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
        add_theme_support('custom-units');
    }
}
add_action('after_setup_theme', 'almalence_setup');

/**
 * Register widget area.
 *
 * @return void
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @since Almalence 1.0
 *
 */
function almalence_widgets_init()
{

    register_sidebar(
        array(
            'name' => esc_html__('Footer', 'almalence'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'almalence'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'almalence_widgets_init');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @return void
 * @global int $content_width Content width.
 *
 * @since Almalence 1.0
 *
 */
function almalence_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('almalence_content_width', 750);
}

add_action('after_setup_theme', 'almalence_content_width', 0);

/**
 * Enqueue scripts and styles.
 *
 * @return void
 * @since Almalence 1.0
 *
 */
function almalence_scripts()
{
    // Note, the is_IE global variable is defined by WordPress and is used
    // to detect if the current browser is internet explorer.
    global $is_IE, $wp_scripts;
    if ($is_IE) {
        // If IE 11 or below, use a flattened stylesheet with static values replacing CSS Variables.
        wp_enqueue_style('twenty-twenty-one-style', get_template_directory_uri() . '/assets/css/ie.css', array(), wp_get_theme()->get('Version'));
    } else {
        // If not IE, use the standard stylesheet.
        wp_enqueue_style('twenty-twenty-one-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get('Version'));
    }

    // RTL styles.
    wp_style_add_data('twenty-twenty-one-style', 'rtl', 'replace');

    // Print styles.
    wp_enqueue_style('twenty-twenty-one-print-style', get_template_directory_uri() . '/assets/css/print.css', array(), wp_get_theme()->get('Version'), 'print');

    // Threaded comment reply styles.
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    // Register the IE11 polyfill file.
    wp_register_script(
        'twenty-twenty-one-ie11-polyfills-asset',
        get_template_directory_uri() . '/assets/js/polyfills.js',
        array(),
        wp_get_theme()->get('Version'),
        true
    );

    // Register the IE11 polyfill loader.
    wp_register_script(
        'twenty-twenty-one-ie11-polyfills',
        null,
        array(),
        wp_get_theme()->get('Version'),
        true
    );
    wp_add_inline_script(
        'twenty-twenty-one-ie11-polyfills',
        wp_get_script_polyfill(
            $wp_scripts,
            array(
                'Element.prototype.matches && Element.prototype.closest && window.NodeList && NodeList.prototype.forEach' => 'twenty-twenty-one-ie11-polyfills-asset',
            )
        )
    );

    // Main navigation scripts.
    if (has_nav_menu('primary')) {
        wp_enqueue_script(
            'twenty-twenty-one-primary-navigation-script',
            get_template_directory_uri() . '/assets/js/primary-navigation.js',
            array('twenty-twenty-one-ie11-polyfills'),
            wp_get_theme()->get('Version'),
            true
        );
    }

    // Responsive embeds script.
    wp_enqueue_script(
        'twenty-twenty-one-responsive-embeds-script',
        get_template_directory_uri() . '/assets/js/responsive-embeds.js',
        array('twenty-twenty-one-ie11-polyfills'),
        wp_get_theme()->get('Version'),
        true
    );
}

add_action('wp_enqueue_scripts', 'almalence_scripts');

/**
 * Enqueue block editor script.
 *
 * @return void
 * @since Almalence 1.0
 *
 */
function almalence_block_editor_script()
{

    wp_enqueue_script('almalence-editor', get_theme_file_uri('/assets/js/editor.js'), array('wp-blocks', 'wp-dom'), wp_get_theme()->get('Version'), true);
}

add_action('enqueue_block_editor_assets', 'almalence_block_editor_script');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function almalence_skip_link_focus_fix()
{

    // If SCRIPT_DEBUG is defined and true, print the unminified file.
    if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
        echo '<script>';
        include get_template_directory() . '/assets/js/skip-link-focus-fix.js';
        echo '</script>';
    }

    // The following is minified via `npx terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
    ?>
    <script>
        /(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", (function () {
            var t, e = location.hash.substring(1);
            /^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
        }), !1);
    </script>
    <?php
}

add_action('wp_print_footer_scripts', 'almalence_skip_link_focus_fix');

/** Enqueue non-latin language styles
 *
 * @return void
 * @since Almalence 1.0
 *
 */
function almalence_non_latin_languages()
{
    $custom_css = almalence_get_non_latin_css('front-end');

    if ($custom_css) {
        wp_add_inline_style('twenty-twenty-one-style', $custom_css);
    }
}

add_action('wp_enqueue_scripts', 'almalence_non_latin_languages');

// SVG Icons class.
require get_template_directory() . '/classes/class-twenty-twenty-one-svg-icons.php';

// Custom color classes.
require get_template_directory() . '/classes/class-twenty-twenty-one-custom-colors.php';
new Almalence_Custom_Colors();

// Enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

// Menu functions and filters.
require get_template_directory() . '/inc/menu-functions.php';

// Custom template tags for the theme.
require get_template_directory() . '/inc/template-tags.php';