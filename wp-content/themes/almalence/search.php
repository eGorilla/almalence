<?php
get_header(null, array('title' => 'Search\' results'));

if (have_posts()) {
?>
	<div class="container">
		<h1><?php printf(esc_html__( 'Results for "%s"', 'almalence' ), esc_html( get_search_query() ));?></h1>
        <p><?php printf(esc_html(_n('Founded %d result for your search.', 'Founded %d results for your search.', (int) $wp_query->found_posts, 'almalence')), (int) $wp_query->found_posts);?></p>
	</div>
<?php
    $IDs = [];
    while (have_posts()) : the_post();
        $ID = get_the_ID();
        if ($ID != DUMMY_ID) $IDs[] = $ID;
    endwhile;
    include getInc('posts-list');
} else include getInc('content-none');

get_footer();
