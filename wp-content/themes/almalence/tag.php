<?php
$topic = checkTag2Topic(getArgs('tag'));
if ($topic) header('Location: ' . $topic);

$title = setHeadTitle();
get_header(null, array('title' => $title));

$IDs = [];
while (have_posts()) : the_post();
    $ID = get_the_ID();
    if ($ID != DUMMY_ID) $IDs[] = $ID;
endwhile;
?>
<h1><?=$title;?> <a class="a-view-more active">view all tags</a></h1>
<div class="tags-container"></div>
<script>
    const btnMore = document.querySelector('.a-view-more');
    btnMore.onclick = function () {
        const c = document.querySelector('.tags-container');
        u.wait(c);
        u.active(btnMore, false);
        u.getJSON(URI_INC + 'load-tags.php', {"slug": '<?=get_queried_object()->slug;?>'}, function (response) {
            if (response) {
                c.innerHTML = response;
                u.wait(c, false);
            }
        });
    }
</script>

<?php
if ($IDs) include getInc('posts-list');

get_footer();
?>