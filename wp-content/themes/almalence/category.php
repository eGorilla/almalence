<?php
$title = setHeadTitle();
get_header(null, array('title' => $title));

$IDs = [];
while (have_posts()) : the_post();
    $ID = get_the_ID();
    if ($ID != DUMMY_ID) $IDs[] = $ID;
endwhile;

$a = [];
$cats = get_categories();
foreach($cats as $cat) {
    $ID = $cat->cat_ID;
    $el = ["ID" => $ID, "name" => $cat->name, "slug" => $cat->slug];
    $parent = $cat->parent;
    if ($parent) {
        if (isset($a[$parent])) {
            if (isset($a[$parent]["children"])) $a[$parent]["children"][] = $el;
            else $a[$parent]["children"] = [$el];
        } else $a[$parent] = ["children" => [$el]];
    } else $a[$ID] = isset($a[$ID]) ? array_merge($a[$ID], $el) : $el;
}

echo '<h1>Category: ' . setCategorySelect($a, get_queried_object()->slug) . '</h1>';
if ($IDs) include getInc('posts-list');?>
<script>
    let dropdown = document.querySelector('h1 select');
    dropdown.onchange = function () {
        location.href = "<?=get_option('home');?>/category/" + dropdown.options[dropdown.selectedIndex].value;
    }
</script>
<?php
get_footer();
?>