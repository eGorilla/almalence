<?php
$uri = getURI();
?>
<!doctype html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$args["title"]; ?></title>
    <link rel="stylesheet" href="<?=$uri;?>css/style.css" />
    <!--<link href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />-->
    <script>const URI_TPL = '<?=$uri;?>', URI_INC = '<?=getURI('includes');?>';</script>
    <script src="<?=$uri;?>js/plugins.js"></script>
</head>
<body>
    <div class="wrapper">
        <main>
            <?php if ($args["title"] !== MAIN_PAGE_TITLE) get_template_part('templates/header');?>