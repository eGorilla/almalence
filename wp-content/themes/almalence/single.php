<?php
$topic = checkTag2Topic(getArgs('name'));
if ($topic) header('Location: ' . $topic);

get_header(null, array('title' => setHeadTitle()));
while (have_posts()) :
    the_post();
    get_template_part('templates/single');
endwhile;
get_footer();