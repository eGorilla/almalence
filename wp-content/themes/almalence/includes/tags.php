<?php
get_header(null, array('title' => 'Tags'));
$pins = [];
$query = new WP_Query(array("tag" => 'topic-pin-tags'));
if ($query->posts) {
    $IDs = array();
    $mainID = false;
    $mainOpenID = false;
    $pins = array();

    while ($query->have_posts()) : $query->the_post();
        $pins[] = $post->ID;
    endwhile;
}
$amount = count($pins);

if ($amount) {
    $pins = getRandom($pins, 3);
    $amount = count($pins);
}
?>
<h1>Tags cloud</h1>
<div class="container">
    <section>
        <div class="tags-container">
            <?=renderTagsList();?>
        </div>
    </section>

    <?php if ($amount):?>
    <section>
        <div class="container">
            <h3>Popular tags</h3>
        </div>
        <div class="topic-pins-<?php echo $amount;?>">
            <div class="container">
                <?php
                foreach ($pins as $ID) {
                    $query = new WP_Query(['p' => $ID]);
                    $query->the_post();
                    get_template_part('templates/topic-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                    wp_reset_query();
                }
                ?>
            </div>
        </div>
    </section>
    <?php endif;?>
</div>
<?php get_footer();?>