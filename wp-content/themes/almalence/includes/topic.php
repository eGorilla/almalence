<?php
$topic = $args["t"];

$a = array();
$tags = get_tags(["name__like" => TOPIC_PFX . $topic]);
foreach ($tags as $tag) $a[] = $tag->name;

$query = new WP_Query(array("tag" => implode(',', $a)));
if ($query->posts) {
    $_SESSION["topic"] = $topic;
	$IDs = array();
	$mainID = false;
    $mainOpenID = false;
	$pins = array();

	while ($query->have_posts()) : $query->the_post();
		$IDs[] = $post->ID;
    endwhile;

    $posts = array();
    foreach ($IDs as $ID) {
        $tags = wp_get_post_tags($ID);
        $a = array();
        $pinID = false;
        foreach ($tags as $tag) {
            $name = $tag->name;
            $a[] = $name;
            preg_match('/' . TOPIC_PFX . $topic . '-pin([0-9]*)/', $name, $pin);
            if (count($pin) > 1) $pinID = $pin[1];
        }
        if (in_array(TOPIC_PFX . $topic . '-main', $a)) $mainID = $ID;
        elseif (in_array(TOPIC_PFX . $topic . '-main-open', $a)) {
            $mainID = $ID;
            $mainOpenID = $ID;
        }
        elseif ($pinID) $pins[$pinID] = $ID;
    }

    if ($mainID || $IDs) {
        $title = '';
        if ($mainID) $title = setHeadTitle($mainID);
        else {
            $tag = get_term_by('slug', $topic, 'post_tag');
            $title = $tag->name;
        }
        get_header(null, array('title' => $title));
    }

    if ($mainID) {
        unset($IDs[array_search($mainID, $IDs)]);

        $query = new WP_Query(['p' => $mainID]);
        $query->the_post();

        $tpl = $mainOpenID ? '-open' : '';
        get_template_part('templates/topic-excerpt' . $tpl);
        wp_reset_query();
    }

    if ($pins) {
        $IDs = array_diff($IDs, $pins);
        include getInc('topic-pins');
    }

    if ($IDs) include getInc('posts-list');
} else include getInc('404');