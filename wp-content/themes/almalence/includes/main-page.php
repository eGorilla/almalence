<?php
get_header(false, array('title' => MAIN_PAGE_TITLE));

$heroPins = array();
$query = new WP_Query(array("tag" => 'topic-pin-hero'));
if ($query->posts) {
    while ($query->have_posts()) : $query->the_post();
        $heroPins[] = $post->ID;
    endwhile;
}

$pressPins = array();
$query = new WP_Query(array("tag" => 'topic-pin-press'));
if ($query->posts) {
    while ($query->have_posts()) : $query->the_post();
        $pressPins[] = $post->ID;
    endwhile;
}

$eventsPins = array();
$query = new WP_Query(array("category" => 'news-and-events'));
if ($query->posts) {
    while ($query->have_posts()) : $query->the_post();
        $eventsPins[] = $post->ID;
    endwhile;
}

$techPins = array();
$a = array();
$tags = get_tags(["name__like" => TOPIC_PFX . 'technology']);
foreach ($tags as $tag) $a[] = $tag->name;
$query = new WP_Query(array("tag" => implode(',', $a)));
if ($query->posts) {
    while ($query->have_posts()) : $query->the_post();
        $techPins[] = $post->ID;
    endwhile;
}

$topicPins = array();
$a = array();
$tags = get_tags(["name__like" => '-main']);
foreach ($tags as $tag) {
    if ($tag->name !== TOPIC_PFX . 'technology-main') $a[] = $tag->name;
}
$query = new WP_Query(array("tag" => implode(',', $a)));
if ($query->posts) {
    while ($query->have_posts()) : $query->the_post();
        $topicPins[] = $post->ID;
    endwhile;
}

$IDs = [];
while (have_posts()) : the_post();
    $ID = get_the_ID();
    if ($ID != DUMMY_ID) $IDs[] = $ID;
endwhile;
?>
<section id="s-hero" class="deco-left bright">
    <div class="deco"></div>
    <header class="header">
        <div class="container">
            <?=viewContacts();?>
            <?=viewLogo(); ?>
            <?php get_search_form(); ?>
        </div>
        <div class="container menu">
            <?php get_template_part( 'templates/nav-main' ); ?>
        </div>
    </header>
    <div class="container p-1">
        <div class="press">
            <div class="slider">
                <ul class="list"><?php
                    foreach ($pressPins as $ID) {
                        $query = new WP_Query(['p' => $ID]);
                        $query->the_post();
                        get_template_part('templates/press-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                        wp_reset_query();
                    }
                    ?></ul>
            </div>
        </div>
        <a href="/topic/almalence/" class="about-company">About Almalence</a>
        <div class="about-company-bg"><div class="inner"><img src="<?=getURI()?>images/about-company-bg.svg" alt="" /></div></div>
    </div>
    <div class="p-2">
        <div class="container">
            <div class="bg"><div class="deco"></div></div>
            <div class="slider">
                <ul class="list"><?php
                    foreach ($heroPins as $ID) {
                        $query = new WP_Query(['p' => $ID]);
                        $query->the_post();
                        get_template_part('templates/hero-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                        wp_reset_query();
                    }
                    ?></ul>
                <nav class="nav-steps"></nav>
            </div>
        </div>
    </div>
</section>
<section id="s-partners" class="container">
    <div class="inner p-1">
        <header>
            <h1>Customers and Partners<a href="/customers-and-partners/">See all</a></h1>
        </header>
        <div class="slider">
            <ul class="list"><li><img src="/wp-content/uploads/2021/09/logo-qualcomm.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-arm.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-cadence.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-omnivision.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-khronos.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-vrar.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-xiaomi.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-vivo.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-asus.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-motorola.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-intel.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-huawei.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-smartisan.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-lg.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-acer.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-oneplus.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-sharp.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-htc.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-imagic.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-hdrsoft.svg" alt=""></li><li><img src="/wp-content/uploads/2021/09/logo-oppo.svg" alt=""></li></ul>
        </div>
    </div>
    <div class="inner p-2">
        <header>
            <h1>News and events<a href="/category/news-and-events/">See all</a></h1>
        </header>
        <div class="slider">
            <ul class="list"><?php
                foreach ($eventsPins as $ID) {
                    $query = new WP_Query(['p' => $ID]);
                    $query->the_post();
                    get_template_part('templates/short-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                    wp_reset_query();
                }
                ?></ul>
        </div>
    </div>
</section>
<section id="s-tech" class="container">
    <div class="inner p-1">
        <header>
            <h1>Technology<a href="/topic/technology/">See all</a></h1>
        </header>
        <div class="slider">
            <ul class="list"><?php
                foreach ($techPins as $ID) {
                    $query = new WP_Query(['p' => $ID]);
                    $query->the_post();
                    get_template_part('templates/hero-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                    wp_reset_query();
                }
                ?></ul>
            <nav class="nav-steps"></nav>
        </div>
    </div>
    <div class="inner p-2">
        <header>
            <h1>Another topics<a href="/sitemap/">See all</a></h1>
        </header>
        <div class="slider">
            <ul class="list"><?php
                foreach ($topicPins as $ID) {
                    $query = new WP_Query(['p' => $ID]);
                    $query->the_post();
                    get_template_part('templates/short-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                    wp_reset_query();
                }
                ?></ul>
        </div>
    </div>
</section>
<section id="s-news-feed" class="container">
    <header>
        <h1>News feed<a href="/news-feed/">See all</a></h1>
    </header>
    <?php if ($IDs) include getInc('posts-list');?>
</section>

<script>
    const WMs = ['s-hero', 's-partners', 's-tech', 's-news-feed'];
    let cWM = 0, lockedWM = false;
    const screens = u.arrayDiff(WMs, 's-news-feed');

    console.log();

    function initScreens() {
        const h = u.viewport().h;
        screens.forEach(function (el) {
            document.getElementById(el).style.height = h + 'px';
        });
        for (let n = 0; n < WMs.length; n++) {
            let el = document.getElementById(WMs[n]);
            if (u.checkVisible(el)) cWM = n;
            u.scrollDir = true;
        }
        if (cWM > 0) cWM -= 1;
        setTimeout(watchWM, 100);
    }
    function watchWM() {
        if (lockedWM) return;

        const total = WMs.length;
        let n = cWM + (u.scrollDir ? 1 : -1);
        if (n < 0) n = 0;
        if (n > total -1) n = total -1;

        if (n !== cWM) {
            let el = document.getElementById(WMs[n]);
            if (u.checkVisible(el)) {
                lockedWM = true;
                let h = n === 0 ? -100 : -40;
                u.scrollTo(el, h);
                setTimeout(function () {
                    cWM = n;
                    lockedWM = false;
                }, 1000);
            }
        }
    }
    PLA.push(initScreens);
    WLA.push({"resize": watchWM}, {"scroll": watchWM}, {"resize": initScreens});

    const sliderPress = new CSlider();
    sliderPress.init({
        "duration": 1500,
        "el": document.querySelector('#s-hero .press .slider'),
        "nav": false,
        "show": true,
        "show-delay": 5000/*,
        "initCallback": function () {
            const h = this.el.offsetHeight, str = '<style>#s-hero figure {width: ' + h + 'px; height: ' + h + 'px; margin-left: -' + (h + 100) + 'px;}</style>';
            let c = this.el.querySelector('.styles');
            if (!c) {
                c = document.createElement('div');
                c.className = 'styles';
                this.el.prepend(c);
            }
            c.innerHTML = str;
        }*/
    });

    const sliderHero = new CSlider();
    sliderHero.init({
        "duration": 1500,
        "el": document.querySelector('#s-hero .p-2 .slider')/*,
        "initCallback": function () {
            const h = this.el.offsetHeight, str = '<style>#s-hero figure {width: ' + h + 'px; height: ' + h + 'px; margin-left: -' + (h + 100) + 'px;}</style>';
            let c = this.el.querySelector('.styles');
            if (!c) {
                c = document.createElement('div');
                c.className = 'styles';
                this.el.prepend(c);
            }
            c.innerHTML = str;
        }*/
    });

    const sliderPartners = new CSlider();
    sliderPartners.init({
        "duration": 1500,
        "el": document.querySelector('#s-partners .p-1 .slider')
    });

    const sliderReviews = new CSlider();
    sliderReviews.init({
        "duration": 1500,
        "el": document.querySelector('#s-partners .p-2 .slider')
    });

    const sliderTech = new CSlider();
    sliderTech.init({
        "duration": 1500,
        "el": document.querySelector('#s-tech .p-1 .slider')
    });

    const sliderTopics = new CSlider();
    sliderTopics.init({
        "duration": 1500,
        "el": document.querySelector('#s-tech .p-2 .slider')
    });
</script>
<?php get_footer();?>