<?php
get_header(null, array('title' => 'Sitemap'));
$html = '';
$a = array();
$tags = get_tags(["name__like" => TOPIC_PFX]);
foreach ($tags as $tag) $a[] = $tag->name;

$topics = array();
foreach ($a as $topic) {
    if (substr($topic, -5) === '-main') $topics[] = $topic;
}

$a = array();
foreach ($topics as $topic) {
    $query = new WP_Query(array("tag" => $topic));
    while ($query->have_posts()) : $query->the_post();
        $topic_tag = substr($topic, 0, -5);
        $a[] = ["tag" => $topic_tag, "title" => '<a href="/topic/' . substr($topic_tag, 6) . '/">' . stripslashes($post->post_title) . '</a>'];
    endwhile;
}

foreach ($a as $topic) {
    $html .= '<li><h3>' . $topic["title"] .'</h3>';
    $query = new WP_Query(array("tag" => $topic["tag"]));
    if ($query->have_posts()) {
        $html .= '<ul>';
        while ($query->have_posts()) : $query->the_post();
            $html .= '<li><a href="/' . $post->post_name . '/">' . stripslashes($post->post_title) . '</a></li>';
        endwhile;
        $html .= '</ul>';
    }
    $html .= '</li>';
}
?>
    <h1>Sitemap<span>(all titles are clicable)</span></h1>
    <section>
        <div class="container">
            <nav class="nav-sitemap">
                <h2>Topics</h2>
                <ul><?=$html?></ul>
            </nav>
        </div>
    </section>
<?php
get_footer();
?>