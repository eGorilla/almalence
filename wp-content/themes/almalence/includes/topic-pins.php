<?php
$amount = count($pins);
?>
<div class="topic-pins-<?php echo $amount;?>">
    <div class="container">
<?php
krsort($pins);
foreach ($pins as $ID) {
	$query = new WP_Query(['p' => $ID]);
	$query->the_post();
	get_template_part('templates/topic-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
	wp_reset_query();
}
?>
    </div>
</div>