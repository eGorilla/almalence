<?php
get_header(null, array('title' => 'Categories'));
$pins = [];
$query = new WP_Query(array("tag" => 'topic-pin-categories'));
if ($query->posts) {
    $IDs = array();
    $mainID = false;
    $mainOpenID = false;
    $pins = array();

    while ($query->have_posts()) : $query->the_post();
        $pins[] = $post->ID;
    endwhile;
}
$amount = count($pins);

if ($amount) {
    $pins = getRandom($pins, 3);
    $amount = count($pins);
}
?>
<h1>Categories</h1>
<section>
    <div class="container">
        <h3>List of categories</h3>
        <?php
        $a = [];
        $cats = get_categories();
        foreach($cats as $cat) {
            $ID = $cat->cat_ID;
            $el = ["ID" => $ID, "name" => $cat->name, "slug" => $cat->slug];
            $parent = $cat->parent;
            if ($parent) {
                if (isset($a[$parent])) {
                    if (isset($a[$parent]["children"])) $a[$parent]["children"][] = $el;
                    else $a[$parent]["children"] = [$el];
                } else $a[$parent] = ["children" => [$el]];
            } else $a[$ID] = isset($a[$ID]) ? array_merge($a[$ID], $el) : $el;
        }
        echo setCategoryList($a);
        ?>
    </div>
</section>

<?php if ($amount):?>
<section>
    <div class="container">
        <h3>See also</h3>
    </div>
    <div class="topic-pins-<?php echo $amount;?>">
        <div class="container">
            <?php
            foreach ($pins as $ID) {
                $query = new WP_Query(['p' => $ID]);
                $query->the_post();
                get_template_part('templates/topic-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                wp_reset_query();
            }
            ?>
        </div>
    </div>
</section>
<?php endif;?>
<?php get_footer();?>