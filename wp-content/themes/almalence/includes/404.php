<?php
get_header(null, array('title' => '404 - Nothing here'));
?>

<header class="page-header alignwide">
	<h1><?php esc_html_e( 'Nothing here', 'twentytwentyone' ); ?></h1>
</header><!-- .page-header -->

<div class="container">
	<div class="page-content">
		<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentytwentyone' ); ?></p>
		<?php get_search_form(); ?>
	</div><!-- .page-content -->
</div><!-- .error-404 -->

<?php
get_footer();
?>