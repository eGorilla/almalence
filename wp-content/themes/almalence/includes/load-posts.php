<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
$obj = json_decode(html_entity_decode(stripslashes($_GET["obj"])));
$IDs = $obj->IDs;
$page = $obj->page + 1;
$total = count($IDs) > POSTS_PER_PAGE * $page;
$load = true;
include getInc('posts-list');