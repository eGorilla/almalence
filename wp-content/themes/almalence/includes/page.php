<?php if ($args["page"] == 'rules'): ?>
<?php get_header(null, array('title' => 'Rules'));?>
<div class="container">
    <h2>MAIN-PAGE</h2>
    <ul>
        <li>
            <h3>1-st screen</h3>
            <ul>
                <li><strong><?=TOPIC_PFX;?>pin-press</strong> - articles for "Press"</li>
                <li><strong><?=TOPIC_PFX;?>pin-hero</strong> - articles for "Main news"</li>
            </ul>
        </li>
    </ul>

    <h2>TAG-SETS</h2>
    <ul>
        <li><strong><?=TOPIC_PFX;?>pin-categories</strong> - articles for "Categories-page", sorting by random order</li>
        <li><strong><?=TOPIC_PFX;?>pin-search</strong> - articles for "Search-page", sorting by random order</li>
        <li><strong><?=TOPIC_PFX;?>pin-tags</strong> - articles for "Tags-page", sorting by random order</li>
    </ul>

    <h2>TOPIC-PAGE</h2>
    <p><em>{name} - topic' tag</em></p>
    <ul>
        <li><strong><?=TOPIC_PFX;?>{name}-main</strong> - excerpt</li>
        <li><strong><?=TOPIC_PFX;?>{name}-main-open</strong> - open notice</li>
        <li><strong><?=TOPIC_PFX;?>{name}-pin{n}</strong> - pinned articles, {n} - any number, sorting in descending order</li>
        <li><strong><?=TOPIC_PFX;?>{name}</strong> - linking articles, sorting by date in descending order</li>
    </ul>
    <h3>Example</h3>
    <p><em>{name} = "music"</em></p>
    <ul>
        <li>Excerpt - <strong><?=TOPIC_PFX;?>music-main</strong></li>
        <li>Open notice - <strong><?=TOPIC_PFX;?>music-main-open</strong></li>
        <li>Pinned articles in order of views:
            <ol>
                <li><strong><?=TOPIC_PFX;?>music-pin10</strong></li>
                <li><strong><?=TOPIC_PFX;?>music-pin6</strong></li>
                <li><strong><?=TOPIC_PFX;?>music-pin1</strong></li>
            </ol>
        </li>
        <li>Linking articles - <strong><?=TOPIC_PFX;?>music</strong></li>
    </ul>

    <h2>GALLERIES</h2>
    <h3>Code</h3>
    <p>&lt;gallery data-type="<strong>{type}</strong>" data-url="<strong>{url}</strong>" data-labels="<strong>{labels}</strong>"&gt;<strong>{images}</strong>&lt;/gallery&gt;</p>
    <ul>
        <li>Types
            <ul>
                <li>slider</li>
                <li>b-a
                    <ul>
                        <li>b-a</li>
                        <li>b-a-slider</li>
                        <li>b-a-combo</li>
                    </ul>
                </li>
                <!--<li>preview</li>-->
            </ul>
        </li>
        <li>Url - <strong>{year}</strong>/<strong>{month}</strong></li>
        <li>Labels - label' names separated by comma</li>
        <li>Images - image' names separated by comma</li>
    </ul>
    <h3>Examples</h3>
    <p><em>{type} = "slider"</em></p>
    <p>&lt;gallery data-url="2021/07" data-type="slider"&gt;1.jpg,2.jpg&lt;/gallery&gt;</p>
    <gallery data-url="2021/07" data-type="slider">1.jpg,2.jpg</gallery>
    <p><em>{type} = "b-a"</em></p>
    <p>&lt;gallery data-url="2021/07" data-type="b-a"&gt;1.jpg,2.jpg&lt;/gallery&gt;</p>
    <gallery data-url="2021/07" data-type="b-a">1.jpg,2.jpg</gallery>
    <p><em>{type} = "b-a", {labels} = "first,second"</em></p>
    <p>&lt;gallery data-url="2021/07" data-type="b-a" data-labels="first,second"&gt;1.jpg,2.jpg&lt;/gallery&gt;</p>
    <gallery data-url="2021/07" data-type="b-a" data-labels="first,second">1.jpg,2.jpg</gallery>
    <p><em>{type} = "b-a-slider"</em></p>
    <p>&lt;gallery data-url="2021/07" data-type="b-a-slider"&gt;1.jpg,2.jpg&lt;/gallery&gt;</p>
    <gallery data-url="2021/07" data-type="b-a-slider">1.jpg,2.jpg</gallery>
    <p><em>{type} = "b-a-combo"</em></p>
    <p>&lt;gallery data-url="2021/07" data-type="b-a-combo"&gt;1.jpg,2.jpg&lt;/gallery&gt;</p>
    <gallery data-url="2021/07" data-type="b-a-combo">1.jpg,2.jpg</gallery>
</div>
<?php else: $error = true; ?>
<?php endif ?>
