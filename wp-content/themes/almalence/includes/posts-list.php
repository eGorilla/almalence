<?php
$IDs = arrayFill($IDs);
$page = 1;
$posts = new WP_Query(array(
    'post__in' => $IDs,
    'posts_per_page' => POSTS_PER_PAGE
));
if ($posts->have_posts()) {
    $a = [];

    if (!isset($load)) echo '<div class="list-articles"><div class="container">';

    while ($posts->have_posts()) : $posts->the_post();
        get_template_part('templates/posts-list-item');
        $a[] = get_the_ID();
    endwhile;
    wp_reset_postdata();

    if (count($IDs) > POSTS_PER_PAGE) echo viewLinkMorePosts(["ID" => $a[count($a) -1], "IDs" => arrayFill(array_diff($IDs, $a)), "nIDs" => $a, "page" => $page]);
    if (!isset($load)) echo '</div></div>';
}