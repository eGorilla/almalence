<?php
get_header(null, array('title' => '404 - Nothing here'));
$pins = [];
$query = new WP_Query(array("tag" => 'topic-pin-search'));
if ($query->posts) {
    $IDs = array();
    $mainID = false;
    $mainOpenID = false;
    $pins = array();

    while ($query->have_posts()) : $query->the_post();
        $pins[] = $post->ID;
    endwhile;
}
$amount = count($pins);

if ($amount) {
    $pins = getRandom($pins, 3);
    $amount = count($pins);
}
?>
<!-- It looks like nothing was found at this location. Maybe try a search? -->
<h1>Nothing here</h1>
<div class="container">
    <section>
        <h2>Try to use our servises</h2>
        <nav class="nav-services"><ul><li><a href="/tags/"><i class="i-tags" title="Tags cloud"></i><span>Tags cloud</span></a></li><li><a href="/categories/"><i class="i-list" title="Categories"></i><span>Categories</span></a></li><li><a href="/sitemap/"><i class="i-sitemap" title="Site map"></i><span>Site map</span></a></li></ul></nav>
    </section>
<?php if ($amount):?>
    <section>
        <h2>News feed</h2>
        <div class="topic-pins-<?php echo $amount;?>">
            <div class="container">
                <?php
                foreach ($pins as $ID) {
                    $query = new WP_Query(['p' => $ID]);
                    $query->the_post();
                    get_template_part('templates/topic-pin', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
                    wp_reset_query();
                }
                ?>
            </div>
        </div>
    </section>
<?php endif;?>
</div>
<?php get_footer();?>