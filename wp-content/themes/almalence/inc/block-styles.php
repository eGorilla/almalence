<?php
/**
 * Block Styles
 *
 * @link https://developer.wordpress.org/reference/functions/register_block_style/
 *
 * @package WordPress
 * @subpackage Almalence
 * @since Almalence 1.0
 */

if ( function_exists( 'register_block_style' ) ) {
	/**
	 * Register block styles.
	 *
	 * @since Almalence 1.0
	 *
	 * @return void
	 */
	function almalence_register_block_styles() {
		// Columns: Overlap.
		register_block_style(
			'core/columns',
			array(
				'name'  => 'almalence-columns-overlap',
				'label' => esc_html__( 'Overlap', 'almalence' ),
			)
		);

		// Cover: Borders.
		register_block_style(
			'core/cover',
			array(
				'name'  => 'almalence-border',
				'label' => esc_html__( 'Borders', 'almalence' ),
			)
		);

		// Group: Borders.
		register_block_style(
			'core/group',
			array(
				'name'  => 'almalence-border',
				'label' => esc_html__( 'Borders', 'almalence' ),
			)
		);

		// Image: Borders.
		register_block_style(
			'core/image',
			array(
				'name'  => 'almalence-border',
				'label' => esc_html__( 'Borders', 'almalence' ),
			)
		);

		// Image: Frame.
		register_block_style(
			'core/image',
			array(
				'name'  => 'almalence-image-frame',
				'label' => esc_html__( 'Frame', 'almalence' ),
			)
		);

		// Latest Posts: Dividers.
		register_block_style(
			'core/latest-posts',
			array(
				'name'  => 'almalence-latest-posts-dividers',
				'label' => esc_html__( 'Dividers', 'almalence' ),
			)
		);

		// Latest Posts: Borders.
		register_block_style(
			'core/latest-posts',
			array(
				'name'  => 'almalence-latest-posts-borders',
				'label' => esc_html__( 'Borders', 'almalence' ),
			)
		);

		// Media & Text: Borders.
		register_block_style(
			'core/media-text',
			array(
				'name'  => 'almalence-border',
				'label' => esc_html__( 'Borders', 'almalence' ),
			)
		);

		// Separator: Thick.
		register_block_style(
			'core/separator',
			array(
				'name'  => 'almalence-separator-thick',
				'label' => esc_html__( 'Thick', 'almalence' ),
			)
		);

		// Social icons: Dark gray color.
		register_block_style(
			'core/social-links',
			array(
				'name'  => 'almalence-social-icons-color',
				'label' => esc_html__( 'Dark gray', 'almalence' ),
			)
		);
	}
	add_action( 'init', 'almalence_register_block_styles' );
}
