<div class="b-search">
    <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input value="<?php echo get_search_query(); ?>" name="s" /><button type="submit"><i class="i-search"></i></button>
    </form>
    <nav><ul><li class="a-search" title="Show/hide search' form"></a></li><li><a href="/news-feed/"><i class="i-list" title="News feed"></i><span>News feed</span></a></li><li><a href="/tags/"><i class="i-tags" title="Tags cloud"></i><span>Tags cloud</span></a></li><li><a href="/sitemap/"><i class="i-sitemap" title="Site map"></i><span>Site map</span></a></li></ul></nav>
</div>